#!/bin/sh

# Copyright (C) 2023 Simon Josefsson <simon@josefsson.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

set -e

LANG=C

for str in $(cat lists/unique-aramo.txt); do
    pkg=$(echo $str | sed 's,=.*$,,')
    ver=$(echo $str | sed 's,^.*=,,')

    if ! test -d "logs/$pkg/$ver" && ! grep -q "^$pkg\$" lists/disabled-aramo.txt; then
	cat<<EOF
build-${pkg}-${ver}:
  stage: build
  variables:
    PACKAGE: "${pkg}"
    VERSION: "${ver}"
  extends: .build-definition

EOF
    fi
done

for str in $(cat lists/unique-aramo.txt); do
    pkg=$(echo $str | sed s,=.*,,)
    ver=$(echo $str | sed s,.*=,,)

    cat<<EOF
rebuild-${pkg}-${ver}:
  stage: manual
  tags:
  - glr1
  variables:
    PACKAGE: "${pkg}"
    VERSION: "${ver}"
  extends: .build-definition

EOF
done

exit 0
