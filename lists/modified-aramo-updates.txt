apache2=2.4.52-1ubuntu4.4+11.0trisquel3
apt=2.4.8+11.0trisquel3
base-files=1:12ubuntu4.2+11.0trisquel6
brltty=6.5-6~bpo11+1+11.0trisquel0
ca-certificates=20211016+11.0trisquel2
casper=1.470.2+11.0trisquel17
compiz=1:0.9.14.1+22.04.20220820-0ubuntu1+11.0trisquel1
cryptsetup=2:2.4.3-1ubuntu1.1+11.0trisquel1
debootstrap=1.0.126+nmu1ubuntu0.2+11.0trisquel6
deja-dup=42.9-1ubuntu3+11.0trisquel1
distro-info-data=0.52ubuntu0.3+11.0trisquel6
dmidecode=3.3-3ubuntu0.1+11.0trisquel0
dpkg=1.21.1ubuntu2.1+11.0trisquel4
e2fsprogs=1.46.5-2ubuntu1.1+11.0trisquel0
epiphany-browser=42.4-0ubuntu1+11.0trisquel1
evince=42.3-0ubuntu3+11.0trisquel2
expat=2.4.7-1+11.0trisquel0
fribidi=1.0.8-2ubuntu3.1+11.0trisquel0
glibc=2.35-0ubuntu3.1+11.0trisquel1
gnome-boxes=42.2-0ubuntu1+11.0trisquel1
gnome-control-center=1:41.7-0ubuntu0.22.04.6+11.0trisquel2
gnome-flashback=3.44.0-1ubuntu2+11.0trisquel1
gnome-tweaks=42~beta-1ubuntu2+11.0trisquel2
gnupg2=2.2.27-3ubuntu2.1+11.0trisquel0
grub2-unsigned=2.06-2ubuntu14.1+11.0trisquel7
grub2=2.06-2ubuntu7.1+11.0trisquel7
hwloc=2.7.0-2ubuntu1+11.0trisquel1
initramfs-tools=0.140ubuntu13.1+11.0trisquel2
isc-dhcp=4.4.1-2.3ubuntu2.4+11.0trisquel2
json-c=0.15-3~ubuntu1.22.04.1+11.0trisquel0
kbd=2.3.0-3ubuntu4.22.04+11.0trisquel1
language-selector=0.219.1+11.0trisquel1
lighttpd=1.4.63-1ubuntu3.1+11.0trisquel1
linux-hwe-5.19=5.19.0-38.39~22.04.1+11.0trisquel19
linux-meta=5.15.0.56.54+11.0trisquel6
linux-meta=5.15.0.60.58+11.0trisquel6
linux-meta=5.15.0.69.67+11.0trisquel6
linux=5.15.0-37.39+11.0trisquel11
linux=5.15.0-40.43+11.0trisquel11
linux=5.15.0-41.44+11.0trisquel11
linux=5.15.0-43.46+11.0trisquel11
linux=5.15.0-46.49+11.0trisquel11
linux=5.15.0-47.51+11.0trisquel11
linux=5.15.0-48.54+11.0trisquel11
linux=5.15.0-50.56+11.0trisquel11
linux=5.15.0-52.58+11.0trisquel13
linux=5.15.0-53.59+11.0trisquel13
linux=5.15.0-56.62+11.0trisquel13
linux=5.15.0-57.63+11.0trisquel13
linux=5.15.0-58.64+11.0trisquel13
linux=5.15.0-60.66+11.0trisquel16
linux=5.15.0-67.74+11.0trisquel18
linux=5.15.0-69.76+11.0trisquel19
mplayer=2:1.4+ds1-3ubuntu0.1+11.0trisquel1
multipath-tools=0.8.8-1ubuntu1.22.04.1+11.0trisquel2
nautilus=1:42.2-0ubuntu2.1+11.0trisquel3
network-manager=1.36.6-0ubuntu2+11.0trisquel2
nginx=1.18.0-6ubuntu14.3+11.0trisquel1
ntfs-3g=1:2021.8.22-3ubuntu1.2+11.0trisquel1
openssh=1:8.9p1-3+11.0trisquel2
openssl=3.0.2-0ubuntu1.8+11.0trisquel0
orca=42.0-1ubuntu2+11.0trisquel1
osinfo-db=0.20220214-1ubuntu2.1+11.0trisquel1
pcre2=10.39-3ubuntu0.1+11.0trisquel0
pcre3=2:8.39-13ubuntu0.22.04.1+11.0trisquel0
plasma-desktop=4:5.24.7-0ubuntu0.1+11.0trisquel1
plasma-discover=5.24.7-0ubuntu0.1+11.0trisquel2
plasma-workspace=4:5.24.7-0ubuntu0.1+11.0trisquel3
pulseaudio=1:15.99.1+dfsg1-1ubuntu2.1+11.0trisquel1
python-apt=2.4.0+11.0trisquel4
quassel=1:0.14.0-1ubuntu0.22.04.4+11.0trisquel3
software-properties=0.99.22.6+11.0trisquel12
systemd=249.11-0ubuntu3.9+11.0trisquel1
thunderbird=1:102.10.0+build2-0ubuntu0.22.04.1+11.0trisquel18
ubiquity=22.04.19+11.0trisquel33
ubuntu-release-upgrader=1:22.04.16+11.0trisquel14
unzip=6.0-26ubuntu3.1+11.0trisquel1
update-manager=1:22.04.10+11.0trisquel17
update-notifier=3.192.54.6+11.0trisquel9
wireless-regdb=2022.06.06-1+11.0trisquel1
wpa=2:2.10-6ubuntu2+11.0trisquel1
xserver-xorg-video-ati=1:19.1.0-2ubuntu1+11.0trisquel1
zlib=1:1.2.11.dfsg-2ubuntu9.2+11.0trisquel0
