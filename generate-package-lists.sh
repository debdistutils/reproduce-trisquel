#!/bin/sh

# Copyright (C) 2023 Simon Josefsson <simon@josefsson.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

set -e
set -x

LANG=C
LC_ALL=C

DEBDISTDIFF=${DEBDISTDIFF:-debdistdiff}

DISTS_UBUNTU=${DISTS_UBUNTU:-/tmp/ubuntu}
DISTS_TRISQUEL=${DISTS_TRISQUEL:-/tmp/trisquel}

if ! test -e $DISTS_UBUNTU; then
    git clone --depth 1 https://gitlab.com/debdistutils/dists/ubuntu.git $DISTS_UBUNTU
fi

if ! test -e $DISTS_TRISQUEL; then
    git clone --depth 1 https://gitlab.com/debdistutils/dists/trisquel.git $DISTS_TRISQUEL
fi

if ! command -v "$DEBDISTDIFF"; then
    git clone --depth 1 https://gitlab.com/debdistutils/debdistdiff.git /tmp/debdistdiff
    DEBDISTDIFF=/tmp/debdistdiff/debdistdiff
fi

mkdir -p lists
ARGS="--modified-sources --arch amd64 --dist $DISTS_UBUNTU --dist $DISTS_TRISQUEL"
$DEBDISTDIFF $ARGS jammy:main,universe           aramo           > lists/modified-aramo.txt
$DEBDISTDIFF $ARGS jammy-updates:main,universe   aramo-updates   > lists/modified-aramo-updates.txt
$DEBDISTDIFF $ARGS jammy-security:main,universe  aramo-security  > lists/modified-aramo-security.txt
$DEBDISTDIFF $ARGS jammy-backports:main,universe aramo-backports > lists/modified-aramo-backports.txt

ARGS="--added-sources --arch amd64 --dist $DISTS_UBUNTU --dist $DISTS_TRISQUEL"
$DEBDISTDIFF $ARGS jammy:main,universe           aramo           > lists/added-aramo.txt
$DEBDISTDIFF $ARGS jammy-updates:main,universe   aramo-updates   > lists/added-aramo-updates.txt
$DEBDISTDIFF $ARGS jammy-security:main,universe  aramo-security  > lists/added-aramo-security.txt
$DEBDISTDIFF $ARGS jammy-backports:main,universe aramo-backports > lists/added-aramo-backports.txt

cat lists/modified-aramo*.txt | sort | uniq > lists/modified-trisquel.txt

cat lists/added-aramo*.txt | sort | uniq > lists/added-trisquel.txt

cat lists/added-trisquel.txt lists/modified-trisquel.txt | sort | uniq > lists/unique-aramo.txt

for d in jammy jammy-updates jammy-security jammy-backports; do
    grep ^Date: $DISTS_UBUNTU/dists/$d/Release | sed 's,Date: ,,' > lists/timestamp-$d.txt
done

for d in aramo aramo-updates aramo-security aramo-backports; do
    grep ^Date: $DISTS_TRISQUEL/dists/$d/Release | sed 's,Date: ,,' > lists/timestamp-$d.txt
done

exit 0
