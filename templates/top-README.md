# Reproducible builds of Trisquel GNU/Linux

This project generate [build
reproducability](https://reproducible-builds.org/) status for
[Trisquel](https://trisquel.info).  Currently we cover Trisquel 11
aramo on amd64.

[[_TOC_]]

## Goals

- Enumerate what packages have been modified in, or added to, Trisquel
  compared to Ubuntu (see
  [debdistdiff](https://gitlab.com/debdistutils/debdistdiff)).
  
- Build the packages in a clean environment.

- Produce diffoscope outputs and reproducability statistics.

## License

All files are published under the AGPLv3+ see the file
[COPYING](COPYING) or [https://www.gnu.org/licenses/agpl-3.0.en.html].

## Status
