#!/bin/sh

# Copyright (C) 2023 Simon Josefsson <simon@josefsson.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

set -e

for pkg in $(cd logs && ls); do
    for ver in $(cd logs/$pkg && ls); do
	grep -q "^$pkg=$ver$" lists/unique-aramo.txt && continue
	echo logs/$pkg/$ver/
    done
    grep -q "^$pkg=" lists/unique-aramo.txt && continue
    echo logs/$pkg/
done


for pkg in $(cd diffoscope && ls); do
    for ver in $(cd diffoscope/$pkg && ls); do
	grep -q "^$pkg=$ver$" lists/unique-aramo.txt && continue
	echo diffoscope/$pkg/$ver/
    done
    grep -q "^$pkg=" lists/unique-aramo.txt && continue
    echo diffoscope/$pkg/
done

# Check that a package cannot both fail to build and build
# reproducible, and vice versa.

find logs/ -name ftbfs.txt | sed s,ftbfs,reproducible, | xargs ls -l 2>/dev/null
find logs/ -name reproducible.txt | sed s,reproducible,ftbfs, | xargs ls -l 2>/dev/null

exit 0
