#!/bin/sh

# Copyright (C) 2023 Simon Josefsson <simon@josefsson.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

set -e

LANG=C
export LANG
LC_ALL=C
export LC_ALL
LC_TIME=C
export LC_TIME
TZ=UTC0
export TZ

TOTAL=$(cat lists/unique-aramo.txt | wc -l)
UNIQUEPKGS=$(cat lists/unique-aramo.txt | sed 's,=.*,,' | sort | uniq | wc -l)
LOGGED=$(find logs/ -name buildlog.txt | wc -l)
FTBFS=$(find logs -name ftbfs.txt | wc -l)
REPRODUCIBLE=$(find logs -name reproducible.txt | wc -l)
UNREPRODUCIBLE=$(expr $LOGGED - $FTBFS - $REPRODUCIBLE)
DISABLED=$(grep -v -e '^#' -e '^\s*$' lists/disabled-aramo.txt | wc -l)
REST=$(expr $TOTAL - $REPRODUCIBLE - $UNREPRODUCIBLE - $FTBFS - $DISABLED || true)
RELEVANTLOGGED=$(cat lists/unique-aramo.txt | sed 's,\(.*\)=\(.*\)$,logs/\1/\2/buildlog.txt,' | xargs ls 2> /dev/null | wc -l)
RELEVANTREPRODUCIBLE=$(cat lists/unique-aramo.txt | sed 's,\(.*\)=\(.*\)$,logs/\1/\2/reproducible.txt,' | xargs ls 2> /dev/null | wc -l)
PERCENT_BUILT=$(echo "100 * ($RELEVANTLOGGED / $TOTAL)" | bc -l | cut -d. -f1)
PERCENT_REPRODUCIBLE=$(echo "100 * ($RELEVANTREPRODUCIBLE / $TOTAL)" | bc -l | cut -d. -f1)
PERCENT_SOFAR=$(echo "100 * ($RELEVANTREPRODUCIBLE / $RELEVANTLOGGED)" | bc -l | cut -d. -f1)
CURRENT=$(date -u --date="@$(cat lists/timestamp-*|xargs -I{} date --date="{}" +%s|sort|tail -1)")

cat templates/top-README.md

cat<<EOF

**Trisquel GNU/Linux 11.0 aramo** (on amd64) currently contains binary
packages, that were added or modified compared to what is currently in
**Ubuntu 22.04 jammy** (on amd64), corresponding to **$UNIQUEPKGS**
source packages.  Some binary packages exists in more than one
version, and there is a total of **$TOTAL** source packages that needs
to be rebuilt to reproduce all added/modified binary packages.  Of
these, we have built **$RELEVANTREPRODUCIBLE** reproducibly out of
**$RELEVANTLOGGED** builds.

We have build logs for **$LOGGED** builds, which may exceed the number
of total source packages to rebuild when a particular source package
(or source package version) has been removed from the archive.  Of the
packages we built, **$REPRODUCIBLE** packages are reproducible and
there are **$UNREPRODUCIBLE** packages that we could not reproduce.
Building **$FTBFS** package had build failures.

We have verified reproducability of **$PERCENT_REPRODUCIBLE%** still
current source packages coming from Trisquel 11 aramo! That is
**$PERCENT_SOFAR%** of the still relevant packages that we have built.
We currently have built **$PERCENT_BUILT%** of all packages.

### Timestamps

The timestamps of the Trisquel/Ubuntu archives used as input to find
out which packages to reproduce are as follows.  To be precise, these
are the \`Date\` field in the respectively \`Release\` file used to
construct the list of packages to evaluate.

When speaking about "current" status of this effort it makes sense to
use the latest timestamp from the set below, which is **$CURRENT**.

| Suite | Ubuntu | Trisquel |
| ----- | ------ | -------- |
EOF

echo "| * | **$(cat lists/timestamp-jammy.txt)** | **$(cat lists/timestamp-aramo.txt)** |"
echo "| *-updates | **$(cat lists/timestamp-jammy-updates.txt)** | **$(cat lists/timestamp-aramo-updates.txt)** |"
echo "| *-security | **$(cat lists/timestamp-jammy-security.txt)** | **$(cat lists/timestamp-aramo-security.txt)** |"
echo "| *-backports | **$(cat lists/timestamp-jammy-backports.txt)** | **$(cat lists/timestamp-aramo-backports.txt)** |"

cat<<EOF

### Unreproducible packages

The following **$UNREPRODUCIBLE** packages unfortunately differ in
some way compared to the version distributed in the Trisquel archive.
Please investigate the build log and
[diffoscope](https://diffoscope.org) output and help us fix it!

| Package | Version | Diffoscope output | Link to build log |
| ------- | ------- | ----------------- | ----------------- |
EOF

for f in $(ls logs/*/*/buildlog.txt | sort); do
    pkg=$(echo $f | sed 's,logs/\([^/]*\)/.*,\1,')
    ver=$(echo $f | sed 's,logs/[^/]*/\([^/]*\)/.*,\1,')
    timestamp=$(head -2 $f | tail -1)
    test -f logs/$pkg/$ver/ftbfs.txt && continue
    test -f logs/$pkg/$ver/reproducible.txt && continue
    if test -f diffoscope/$pkg/$ver/index.html; then
	echo "| [$pkg](https://packages.trisquel.info/search?keywords=$pkg&searchon=sourcenames&suite=all&section=all) | $ver | [diffoscope output](https://debdistutils.gitlab.io/reproduce-trisquel/diffoscope/$pkg/$ver/index.html) | [build log from $timestamp](https://gitlab.com/debdistutils/reproduce-trisquel/-/blob/main/logs/$pkg/$ver/buildlog.txt) |"
    else
	echo "| [$pkg](https://packages.trisquel.info/search?keywords=$pkg&searchon=sourcenames&suite=all&section=all) | $ver | no diffoscope output (probably too large) | [build log from $timestamp](https://gitlab.com/debdistutils/reproduce-trisquel/-/blob/main/logs/$pkg/$ver/buildlog.txt) |"
    fi
done

cat<<EOF

### Build failures

The following **$FTBFS** packages have build failures, making it
impossible to even compare the binary package in the Trisquel archive
with what we are able to build locally.

#### Missing source code

The archive is missing source code for the following source packages,
for which the archive is shipping a binary packages that claims it was
built using that particular source package/version.

| Package | Version | Link to build log |
| ------- | ------- | ----------------- |
EOF

for f in logs/*/*/ftbfs.txt; do
    pkg=$(echo $f | sed 's,logs/\([^/]*\)/.*,\1,')
    ver=$(echo $f | sed 's,logs/[^/]*/\([^/]*\)/.*,\1,')
    buildlog=$(echo $f | sed 's,/ftbfs.txt,/buildlog.txt,')
    (tail -n1 $buildlog | grep -q '^E: Unable to find a source package for' && grep -q "^$pkg=$ver\$" lists/unique-aramo.txt) || continue
    timestamp=$(head -2 $buildlog | tail -1)
    echo "| [$pkg](https://packages.trisquel.info/search?keywords=$pkg&searchon=sourcenames&suite=all&section=all) | $ver | [build log from $timestamp](https://gitlab.com/debdistutils/reproduce-trisquel/-/blob/main/logs/$pkg/$ver/buildlog.txt) |"
done    

cat<<EOF

#### Other build failures

Please investigate the build log and help us fix it!

| Package | Version | Link to build log |
| ------- | ------- | ----------------- |
EOF

for f in logs/*/*/ftbfs.txt; do
    pkg=$(echo $f | sed 's,logs/\([^/]*\)/.*,\1,')
    ver=$(echo $f | sed 's,logs/[^/]*/\([^/]*\)/.*,\1,')
    buildlog=$(echo $f | sed 's,/ftbfs.txt,/buildlog.txt,')
    (tail -n1 $buildlog | grep -q '^E: Unable to find a source package for' && grep -q "^$pkg=$ver\$" lists/unique-aramo.txt) && continue
    timestamp=$(head -2 $buildlog | tail -1)
    echo "| [$pkg](https://packages.trisquel.info/source/aramo/$pkg) | $ver | [build log from $timestamp](https://gitlab.com/debdistutils/reproduce-trisquel/-/blob/main/logs/$pkg/$ver/buildlog.txt) |"
done    

cat<<EOF

### Reproducible packages

The following **$REPRODUCIBLE** packages can be built locally to
produce the exact same package that is shipped in the Trisquel
archive.

| Package | Version | Link to build log |
| ------- | ------- | ----------------- |
EOF

for f in logs/*/*/reproducible.txt; do
    pkg=$(echo $f | sed 's,logs/\([^/]*\)/.*,\1,')
    ver=$(echo $f | sed 's,logs/[^/]*/\([^/]*\)/.*,\1,')
    buildlog=$(echo $f | sed 's,/reproducible.txt,/buildlog.txt,')
    timestamp=$(head -2 $buildlog | tail -1)
    echo "| [$pkg](https://packages.trisquel.info/search?keywords=$pkg&searchon=sourcenames&suite=all&section=all) | $ver | [build log from $timestamp](https://gitlab.com/debdistutils/reproduce-trisquel/-/blob/main/logs/$pkg/$ver/buildlog.txt) |"
done

cat<<EOF

### Manually disabled packages

Currently the following **$DISABLED** packages are being held back
from building manually.  Usually the reason is high computational
resoure demands; see
[lists/disabled-aramo.txt](lists/disabled-aramo.txt).

EOF

for pkg in $(grep -v -e ^# lists/disabled-aramo.txt | sort); do
    test -d logs/$pkg && continue
    echo "- [$pkg](https://packages.trisquel.info/source/aramo/$pkg)"
done

if test "$REST" -gt 0; then
    cat<<EOF

### Packages that remains to be built

The following **$REST** packages have not yet been built.

EOF

    for pkg in $(cat lists/unique-aramo.txt); do
	test -d logs/$pkg && continue
	grep -q "^$pkg\$" lists/disabled-aramo.txt && continue
	echo "- [$pkg](https://packages.trisquel.info/search?keywords=$pkg&searchon=sourcenames&suite=all&section=all)"
    done
fi

cat templates/bottom-README.md

exit 0
