+ date
Tue Apr 11 19:23:25 UTC 2023
+ apt-get source --only-source meta-gnome3=1:42+3+11.0trisquel3
Reading package lists...
NOTICE: 'meta-gnome3' packaging is maintained in the 'Git' version control system at:
https://salsa.debian.org/gnome-team/meta-gnome3.git
Please use:
git clone https://salsa.debian.org/gnome-team/meta-gnome3.git
to retrieve the latest (possibly unreleased) updates to the package.
Need to get 32.8 kB of source archives.
Get:1 http://archive.trisquel.info/trisquel aramo/main meta-gnome3 1:42+3+11.0trisquel3 (dsc) [2124 B]
Get:2 http://archive.trisquel.info/trisquel aramo/main meta-gnome3 1:42+3+11.0trisquel3 (tar) [30.6 kB]
dpkg-source: info: extracting meta-gnome3 in meta-gnome3-42+3+11.0trisquel3
dpkg-source: info: unpacking meta-gnome3_42+3+11.0trisquel3.tar.gz
Fetched 32.8 kB in 0s (238 kB/s)
W: Download is performed unsandboxed as root as file 'meta-gnome3_42+3+11.0trisquel3.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source meta-gnome3=1:42+3+11.0trisquel3
Reading package lists...
Building dependency tree...
Reading state information...
The following NEW packages will be installed:
  autoconf automake autopoint autotools-dev debhelper debugedit dh-autoreconf
  dh-strip-nondeterminism dh-translations dwz gir1.2-freedesktop
  gir1.2-gdkpixbuf-2.0 gir1.2-rsvg-2.0 gnome-pkg-tools intltool jq
  libdebhelper-perl libfile-stripnondeterminism-perl libjq1 libonig5
  libsub-override-perl libtool m4 po-debconf python3-cairo python3-gi-cairo
  python3-scour scour
0 upgraded, 28 newly installed, 0 to remove and 11 not upgraded.
Need to get 3780 kB of archives.
After this operation, 11.1 MB of additional disk space will be used.
Get:1 http://archive.trisquel.info/trisquel aramo/main amd64 m4 amd64 1.4.18-5ubuntu2 [199 kB]
Get:2 http://archive.trisquel.info/trisquel aramo/main amd64 autoconf all 2.71-2 [338 kB]
Get:3 http://archive.trisquel.info/trisquel aramo/main amd64 autotools-dev all 20220109.1 [44.9 kB]
Get:4 http://archive.trisquel.info/trisquel aramo/main amd64 automake all 1:1.16.5-1.3 [558 kB]
Get:5 http://archive.trisquel.info/trisquel aramo/main amd64 autopoint all 0.21-4ubuntu4 [422 kB]
Get:6 http://archive.trisquel.info/trisquel aramo/main amd64 libdebhelper-perl all 13.6ubuntu1 [67.2 kB]
Get:7 http://archive.trisquel.info/trisquel aramo/main amd64 libtool all 2.4.6-15build2 [164 kB]
Get:8 http://archive.trisquel.info/trisquel aramo/main amd64 dh-autoreconf all 20 [16.1 kB]
Get:9 http://archive.trisquel.info/trisquel aramo/main amd64 libsub-override-perl all 0.09-2 [9532 B]
Get:10 http://archive.trisquel.info/trisquel aramo/main amd64 libfile-stripnondeterminism-perl all 1.13.0-1 [18.1 kB]
Get:11 http://archive.trisquel.info/trisquel aramo/main amd64 dh-strip-nondeterminism all 1.13.0-1 [5344 B]
Get:12 http://archive.trisquel.info/trisquel aramo/main amd64 debugedit amd64 1:5.0-4build1 [47.2 kB]
Get:13 http://archive.trisquel.info/trisquel aramo/main amd64 dwz amd64 0.14-1build2 [105 kB]
Get:14 http://archive.trisquel.info/trisquel aramo/main amd64 po-debconf all 1.0.21+nmu1 [233 kB]
Get:15 http://archive.trisquel.info/trisquel aramo/main amd64 debhelper all 13.6ubuntu1 [923 kB]
Get:16 http://archive.trisquel.info/trisquel aramo/main amd64 gir1.2-freedesktop amd64 1.72.0-1 [22.3 kB]
Get:17 http://archive.trisquel.info/trisquel aramo-updates/main amd64 gir1.2-gdkpixbuf-2.0 amd64 2.42.8+dfsg-1ubuntu0.2 [9482 B]
Get:18 http://archive.trisquel.info/trisquel aramo/main amd64 gir1.2-rsvg-2.0 amd64 2.52.5+dfsg-3 [16.5 kB]
Get:19 http://archive.trisquel.info/trisquel aramo/main amd64 intltool all 0.51.0-6 [44.6 kB]
Get:20 http://archive.trisquel.info/trisquel aramo/main amd64 libonig5 amd64 6.9.7.1-2build1 [172 kB]
Get:21 http://archive.trisquel.info/trisquel aramo/main amd64 libjq1 amd64 1.6-2.1ubuntu3 [133 kB]
Get:22 http://archive.trisquel.info/trisquel aramo/main amd64 jq amd64 1.6-2.1ubuntu3 [52.5 kB]
Get:23 http://archive.trisquel.info/trisquel aramo/main amd64 dh-translations all 149 [25.2 kB]
Get:24 http://archive.trisquel.info/trisquel aramo/main amd64 python3-cairo amd64 1.20.1-3build1 [73.7 kB]
Get:25 http://archive.trisquel.info/trisquel aramo-updates/main amd64 python3-gi-cairo amd64 3.42.1-0ubuntu1 [8184 B]
Get:26 http://archive.trisquel.info/trisquel aramo/main amd64 python3-scour all 0.38.2-2 [49.4 kB]
Get:27 http://archive.trisquel.info/trisquel aramo/main amd64 scour all 0.38.2-2 [7778 B]
Get:28 http://archive.trisquel.info/trisquel aramo/main amd64 gnome-pkg-tools all 0.22.6ubuntu1 [16.7 kB]
debconf: delaying package configuration, since apt-utils is not installed
Fetched 3780 kB in 1s (7445 kB/s)
Selecting previously unselected package m4.
(Reading database ... (Reading database ... 5%(Reading database ... 10%(Reading database ... 15%(Reading database ... 20%(Reading database ... 25%(Reading database ... 30%(Reading database ... 35%(Reading database ... 40%(Reading database ... 45%(Reading database ... 50%(Reading database ... 55%(Reading database ... 60%(Reading database ... 65%(Reading database ... 70%(Reading database ... 75%(Reading database ... 80%(Reading database ... 85%(Reading database ... 90%(Reading database ... 95%(Reading database ... 100%(Reading database ... 123238 files and directories currently installed.)
Preparing to unpack .../00-m4_1.4.18-5ubuntu2_amd64.deb ...
Unpacking m4 (1.4.18-5ubuntu2) ...
Selecting previously unselected package autoconf.
Preparing to unpack .../01-autoconf_2.71-2_all.deb ...
Unpacking autoconf (2.71-2) ...
Selecting previously unselected package autotools-dev.
Preparing to unpack .../02-autotools-dev_20220109.1_all.deb ...
Unpacking autotools-dev (20220109.1) ...
Selecting previously unselected package automake.
Preparing to unpack .../03-automake_1%3a1.16.5-1.3_all.deb ...
Unpacking automake (1:1.16.5-1.3) ...
Selecting previously unselected package autopoint.
Preparing to unpack .../04-autopoint_0.21-4ubuntu4_all.deb ...
Unpacking autopoint (0.21-4ubuntu4) ...
Selecting previously unselected package libdebhelper-perl.
Preparing to unpack .../05-libdebhelper-perl_13.6ubuntu1_all.deb ...
Unpacking libdebhelper-perl (13.6ubuntu1) ...
Selecting previously unselected package libtool.
Preparing to unpack .../06-libtool_2.4.6-15build2_all.deb ...
Unpacking libtool (2.4.6-15build2) ...
Selecting previously unselected package dh-autoreconf.
Preparing to unpack .../07-dh-autoreconf_20_all.deb ...
Unpacking dh-autoreconf (20) ...
Selecting previously unselected package libsub-override-perl.
Preparing to unpack .../08-libsub-override-perl_0.09-2_all.deb ...
Unpacking libsub-override-perl (0.09-2) ...
Selecting previously unselected package libfile-stripnondeterminism-perl.
Preparing to unpack .../09-libfile-stripnondeterminism-perl_1.13.0-1_all.deb ...
Unpacking libfile-stripnondeterminism-perl (1.13.0-1) ...
Selecting previously unselected package dh-strip-nondeterminism.
Preparing to unpack .../10-dh-strip-nondeterminism_1.13.0-1_all.deb ...
Unpacking dh-strip-nondeterminism (1.13.0-1) ...
Selecting previously unselected package debugedit.
Preparing to unpack .../11-debugedit_1%3a5.0-4build1_amd64.deb ...
Unpacking debugedit (1:5.0-4build1) ...
Selecting previously unselected package dwz.
Preparing to unpack .../12-dwz_0.14-1build2_amd64.deb ...
Unpacking dwz (0.14-1build2) ...
Selecting previously unselected package po-debconf.
Preparing to unpack .../13-po-debconf_1.0.21+nmu1_all.deb ...
Unpacking po-debconf (1.0.21+nmu1) ...
Selecting previously unselected package debhelper.
Preparing to unpack .../14-debhelper_13.6ubuntu1_all.deb ...
Unpacking debhelper (13.6ubuntu1) ...
Selecting previously unselected package gir1.2-freedesktop:amd64.
Preparing to unpack .../15-gir1.2-freedesktop_1.72.0-1_amd64.deb ...
Unpacking gir1.2-freedesktop:amd64 (1.72.0-1) ...
Selecting previously unselected package gir1.2-gdkpixbuf-2.0:amd64.
Preparing to unpack .../16-gir1.2-gdkpixbuf-2.0_2.42.8+dfsg-1ubuntu0.2_amd64.deb ...
Unpacking gir1.2-gdkpixbuf-2.0:amd64 (2.42.8+dfsg-1ubuntu0.2) ...
Selecting previously unselected package gir1.2-rsvg-2.0:amd64.
Preparing to unpack .../17-gir1.2-rsvg-2.0_2.52.5+dfsg-3_amd64.deb ...
Unpacking gir1.2-rsvg-2.0:amd64 (2.52.5+dfsg-3) ...
Selecting previously unselected package intltool.
Preparing to unpack .../18-intltool_0.51.0-6_all.deb ...
Unpacking intltool (0.51.0-6) ...
Selecting previously unselected package libonig5:amd64.
Preparing to unpack .../19-libonig5_6.9.7.1-2build1_amd64.deb ...
Unpacking libonig5:amd64 (6.9.7.1-2build1) ...
Selecting previously unselected package libjq1:amd64.
Preparing to unpack .../20-libjq1_1.6-2.1ubuntu3_amd64.deb ...
Unpacking libjq1:amd64 (1.6-2.1ubuntu3) ...
Selecting previously unselected package jq.
Preparing to unpack .../21-jq_1.6-2.1ubuntu3_amd64.deb ...
Unpacking jq (1.6-2.1ubuntu3) ...
Selecting previously unselected package dh-translations.
Preparing to unpack .../22-dh-translations_149_all.deb ...
Unpacking dh-translations (149) ...
Selecting previously unselected package python3-cairo:amd64.
Preparing to unpack .../23-python3-cairo_1.20.1-3build1_amd64.deb ...
Unpacking python3-cairo:amd64 (1.20.1-3build1) ...
Selecting previously unselected package python3-gi-cairo.
Preparing to unpack .../24-python3-gi-cairo_3.42.1-0ubuntu1_amd64.deb ...
Unpacking python3-gi-cairo (3.42.1-0ubuntu1) ...
Selecting previously unselected package python3-scour.
Preparing to unpack .../25-python3-scour_0.38.2-2_all.deb ...
Unpacking python3-scour (0.38.2-2) ...
Selecting previously unselected package scour.
Preparing to unpack .../26-scour_0.38.2-2_all.deb ...
Unpacking scour (0.38.2-2) ...
Selecting previously unselected package gnome-pkg-tools.
Preparing to unpack .../27-gnome-pkg-tools_0.22.6ubuntu1_all.deb ...
Unpacking gnome-pkg-tools (0.22.6ubuntu1) ...
Setting up gir1.2-freedesktop:amd64 (1.72.0-1) ...
Setting up python3-cairo:amd64 (1.20.1-3build1) ...
Setting up gir1.2-gdkpixbuf-2.0:amd64 (2.42.8+dfsg-1ubuntu0.2) ...
Setting up po-debconf (1.0.21+nmu1) ...
Setting up libdebhelper-perl (13.6ubuntu1) ...
Setting up m4 (1.4.18-5ubuntu2) ...
Setting up autotools-dev (20220109.1) ...
Setting up autopoint (0.21-4ubuntu4) ...
Setting up autoconf (2.71-2) ...
Setting up dwz (0.14-1build2) ...
Setting up debugedit (1:5.0-4build1) ...
Setting up libsub-override-perl (0.09-2) ...
Setting up python3-scour (0.38.2-2) ...
Setting up gir1.2-rsvg-2.0:amd64 (2.52.5+dfsg-3) ...
Setting up python3-gi-cairo (3.42.1-0ubuntu1) ...
Setting up libonig5:amd64 (6.9.7.1-2build1) ...
Setting up automake (1:1.16.5-1.3) ...
update-alternatives: using /usr/bin/automake-1.16 to provide /usr/bin/automake (automake) in auto mode
Setting up libfile-stripnondeterminism-perl (1.13.0-1) ...
Setting up libtool (2.4.6-15build2) ...
Setting up libjq1:amd64 (1.6-2.1ubuntu3) ...
Setting up dh-autoreconf (20) ...
Setting up intltool (0.51.0-6) ...
Setting up dh-strip-nondeterminism (1.13.0-1) ...
Setting up scour (0.38.2-2) ...
Setting up jq (1.6-2.1ubuntu3) ...
Setting up debhelper (13.6ubuntu1) ...
Setting up dh-translations (149) ...
Setting up gnome-pkg-tools (0.22.6ubuntu1) ...
Processing triggers for man-db (2.10.2-1) ...
Processing triggers for libc-bin (2.35-0ubuntu3.1+11.0trisquel1) ...
+ find . -maxdepth 1 -name meta-gnome3* -type d
+ cd ./meta-gnome3-42+3+11.0trisquel3
+ eatmydata env DEB_BUILD_OPTIONS=noautodbgsym dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package meta-gnome3
dpkg-buildpackage: info: source version 1:42+3+11.0trisquel3
dpkg-buildpackage: info: source distribution aramo
dpkg-buildpackage: info: source changed by Trisquel GNU/Linux developers <trisquel-devel@listas.trisquel.info>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 debian/rules clean
dh clean
   dh_gnome_clean
   dh_clean
 dpkg-source -b .
dpkg-source: warning: no source format specified in debian/source/format, see dpkg-source(1)
dpkg-source: info: using source format '1.0'
dpkg-source: info: building meta-gnome3 in meta-gnome3_42+3+11.0trisquel3.tar.gz
dpkg-source: info: building meta-gnome3 in meta-gnome3_42+3+11.0trisquel3.dsc
 debian/rules binary
dh binary
   dh_update_autotools_config
   dh_autoreconf
   create-stamp debian/debhelper-build-stamp
   dh_prep
   dh_installdocs
   dh_installchangelogs
   dh_bugfiles
   debian/rules override_dh_gnome
make[1]: Entering directory '/build/meta-gnome3/meta-gnome3-42+3+11.0trisquel3'
dh_gnome --no-gnome-versions
make[1]: Leaving directory '/build/meta-gnome3/meta-gnome3-42+3+11.0trisquel3'
   dh_perl
   dh_link
   dh_scour
   dh_strip_nondeterminism
   dh_translations
dh_translations: warning: could not determine domain
   dh_compress
   dh_fixperms
   dh_missing
   dh_dwz -a
   dh_strip -a
   dh_makeshlibs -a
   dh_shlibdeps -a
   dh_installdeb
   dh_gencontrol
   dh_md5sums
   dh_builddeb
pkgstripfiles: processing control file: debian/gnome-core/DEBIAN/control, package gnome-core, directory debian/gnome-core
pkgstripfiles: Truncating usr/share/doc/gnome-core/changelog.gz to topmost ten records
pkgstripfiles: Running PNG optimization (using 1 cpus) for package gnome-core ...
pkgstripfiles: No PNG files.
dpkg-deb: building package 'gnome-core' in '../gnome-core_42+3+11.0trisquel3_amd64.deb'.
pkgstripfiles: processing control file: debian/gnome/DEBIAN/control, package gnome, directory debian/gnome
Searching for duplicated docs in dependency gnome-core...
  symlinking changelog.gz in gnome to file in gnome-core
pkgstripfiles: Running PNG optimization (using 1 cpus) for package gnome ...
pkgstripfiles: No PNG files.
dpkg-deb: building package 'gnome' in '../gnome_42+3+11.0trisquel3_amd64.deb'.
pkgstripfiles: processing control file: debian/gnome-games/DEBIAN/control, package gnome-games, directory debian/gnome-games
pkgstripfiles: Truncating usr/share/doc/gnome-games/changelog.gz to topmost ten records
pkgstripfiles: Running PNG optimization (using 1 cpus) for package gnome-games ...
pkgstripfiles: No PNG files.
dpkg-deb: building package 'gnome-games' in '../gnome-games_42+3+11.0trisquel3_all.deb'.
pkgstripfiles: processing control file: debian/gnome-platform-devel/DEBIAN/control, package gnome-platform-devel, directory debian/gnome-platform-devel
pkgstripfiles: Truncating usr/share/doc/gnome-platform-devel/changelog.gz to topmost ten records
pkgstripfiles: Running PNG optimization (using 1 cpus) for package gnome-platform-devel ...
pkgstripfiles: No PNG files.
dpkg-deb: building package 'gnome-platform-devel' in '../gnome-platform-devel_42+3+11.0trisquel3_all.deb'.
pkgstripfiles: processing control file: debian/gnome-devel/DEBIAN/control, package gnome-devel, directory debian/gnome-devel
pkgstripfiles: Truncating usr/share/doc/gnome-devel/changelog.gz to topmost ten records
pkgstripfiles: Running PNG optimization (using 1 cpus) for package gnome-devel ...
pkgstripfiles: No PNG files.
dpkg-deb: building package 'gnome-devel' in '../gnome-devel_42+3+11.0trisquel3_all.deb'.
pkgstripfiles: processing control file: debian/gnome-api-docs/DEBIAN/control, package gnome-api-docs, directory debian/gnome-api-docs
pkgstripfiles: Truncating usr/share/doc/gnome-api-docs/changelog.gz to topmost ten records
pkgstripfiles: Running PNG optimization (using 1 cpus) for package gnome-api-docs ...
pkgstripfiles: No PNG files.
dpkg-deb: building package 'gnome-api-docs' in '../gnome-api-docs_42+3+11.0trisquel3_all.deb'.
 dpkg-genbuildinfo -O../meta-gnome3_42+3+11.0trisquel3_amd64.buildinfo
 dpkg-genchanges -O../meta-gnome3_42+3+11.0trisquel3_amd64.changes
dpkg-genchanges: info: including full source code in upload
 dpkg-source --after-build .
dpkg-buildpackage: info: full upload; Debian-native package (full source is included)
+ date
Tue Apr 11 19:23:57 UTC 2023
+ cd ..
+ ls -la
total 108
drwxr-xr-x 3 root root  4096 Apr 11 19:23 .
drwxr-xr-x 3 root root  4096 Apr 11 19:23 ..
-rw-r--r-- 1 root root 15969 Apr 11 19:23 buildlog.txt
-rw-r--r-- 1 root root  3940 Apr 11 19:23 gnome-api-docs_42+3+11.0trisquel3_all.deb
-rw-r--r-- 1 root root  5080 Apr 11 19:23 gnome-core_42+3+11.0trisquel3_amd64.deb
-rw-r--r-- 1 root root  3854 Apr 11 19:23 gnome-devel_42+3+11.0trisquel3_all.deb
-rw-r--r-- 1 root root  3926 Apr 11 19:23 gnome-games_42+3+11.0trisquel3_all.deb
-rw-r--r-- 1 root root  3924 Apr 11 19:23 gnome-platform-devel_42+3+11.0trisquel3_all.deb
-rw-r--r-- 1 root root  2566 Apr 11 19:23 gnome_42+3+11.0trisquel3_amd64.deb
drwxr-xr-x 3 root root  4096 Apr  2  2022 meta-gnome3-42+3+11.0trisquel3
-rw-r--r-- 1 root root  1241 Apr 11 19:23 meta-gnome3_42+3+11.0trisquel3.dsc
-rw-r--r-- 1 root root 30648 Apr 11 19:23 meta-gnome3_42+3+11.0trisquel3.tar.gz
-rw-r--r-- 1 root root 11642 Apr 11 19:23 meta-gnome3_42+3+11.0trisquel3_amd64.buildinfo
-rw-r--r-- 1 root root  3628 Apr 11 19:23 meta-gnome3_42+3+11.0trisquel3_amd64.changes
+ find . -maxdepth 1 -type f
+ sha256sum ./meta-gnome3_42+3+11.0trisquel3.tar.gz ./gnome-core_42+3+11.0trisquel3_amd64.deb ./buildlog.txt ./meta-gnome3_42+3+11.0trisquel3_amd64.buildinfo ./gnome-devel_42+3+11.0trisquel3_all.deb ./gnome-platform-devel_42+3+11.0trisquel3_all.deb ./gnome-games_42+3+11.0trisquel3_all.deb ./meta-gnome3_42+3+11.0trisquel3_amd64.changes ./meta-gnome3_42+3+11.0trisquel3.dsc ./gnome_42+3+11.0trisquel3_amd64.deb ./gnome-api-docs_42+3+11.0trisquel3_all.deb
55a4a42115c62303973df172d708e439be0fb33f926a7f42ce138e0af6d8c7ea  ./meta-gnome3_42+3+11.0trisquel3.tar.gz
c733d4206082640c5f60fa52e7b569a643c73c880f0a35e5ceec9c20fe0c2abf  ./gnome-core_42+3+11.0trisquel3_amd64.deb
b56e5b69cf81ea05bae17de56f62ec78cc0bf7630eeef9e45385caa49ff41c19  ./buildlog.txt
d60d2d43f0f4314203c5cef85890c13ae1846fc461863890727b8a7005285c2d  ./meta-gnome3_42+3+11.0trisquel3_amd64.buildinfo
1f2c342063414f2aa40473c265678f0eaca654f1adb7252c6b2621bc811327e6  ./gnome-devel_42+3+11.0trisquel3_all.deb
489d24353c88b25ef886f6e66aecae90d209d6baa38aaea52cc3415bd85c0ca5  ./gnome-platform-devel_42+3+11.0trisquel3_all.deb
0f3c23b1413b273265fea1a10ff0114346873e261cdf74b6f372b248322cb57f  ./gnome-games_42+3+11.0trisquel3_all.deb
d67ecb36fc58ea5b41287a6f7b45ae927fee46e53b5088ea321ae50c26771216  ./meta-gnome3_42+3+11.0trisquel3_amd64.changes
3849840d4c32224dc99284febe4cd01a3e46779884e983c60b37bdaeb24b4723  ./meta-gnome3_42+3+11.0trisquel3.dsc
8d6076728d6b509e41ab7fa174a3084f309d06b0ab51c37d6bd82f6c7e1d1c7d  ./gnome_42+3+11.0trisquel3_amd64.deb
8adb67d71d943e9fc48b576eee04ab63af967b12e2316da0010565f726d3d7ce  ./gnome-api-docs_42+3+11.0trisquel3_all.deb
+ mkdir published
+ cd published
+ cd ../
+ ls gnome-api-docs_42+3+11.0trisquel3_all.deb gnome-core_42+3+11.0trisquel3_amd64.deb gnome-devel_42+3+11.0trisquel3_all.deb gnome-games_42+3+11.0trisquel3_all.deb gnome-platform-devel_42+3+11.0trisquel3_all.deb gnome_42+3+11.0trisquel3_amd64.deb *.udeb
ls: cannot access '*.udeb': No such file or directory
+ wget -q http://archive.trisquel.info/trisquel/pool/main/m/meta-gnome3/gnome-api-docs_42+3+11.0trisquel3_all.deb
+ wget -q http://archive.trisquel.info/trisquel/pool/main/m/meta-gnome3/gnome-core_42+3+11.0trisquel3_amd64.deb
+ wget -q http://archive.trisquel.info/trisquel/pool/main/m/meta-gnome3/gnome-devel_42+3+11.0trisquel3_all.deb
+ wget -q http://archive.trisquel.info/trisquel/pool/main/m/meta-gnome3/gnome-games_42+3+11.0trisquel3_all.deb
+ wget -q http://archive.trisquel.info/trisquel/pool/main/m/meta-gnome3/gnome-platform-devel_42+3+11.0trisquel3_all.deb
+ wget -q http://archive.trisquel.info/trisquel/pool/main/m/meta-gnome3/gnome_42+3+11.0trisquel3_amd64.deb
+ tee ../SHA256SUMS
+ find . -maxdepth 1 -type f
+ sha256sum ./gnome-core_42+3+11.0trisquel3_amd64.deb ./gnome-devel_42+3+11.0trisquel3_all.deb ./gnome-platform-devel_42+3+11.0trisquel3_all.deb ./gnome-games_42+3+11.0trisquel3_all.deb ./gnome_42+3+11.0trisquel3_amd64.deb ./gnome-api-docs_42+3+11.0trisquel3_all.deb
c733d4206082640c5f60fa52e7b569a643c73c880f0a35e5ceec9c20fe0c2abf  ./gnome-core_42+3+11.0trisquel3_amd64.deb
1f2c342063414f2aa40473c265678f0eaca654f1adb7252c6b2621bc811327e6  ./gnome-devel_42+3+11.0trisquel3_all.deb
489d24353c88b25ef886f6e66aecae90d209d6baa38aaea52cc3415bd85c0ca5  ./gnome-platform-devel_42+3+11.0trisquel3_all.deb
0f3c23b1413b273265fea1a10ff0114346873e261cdf74b6f372b248322cb57f  ./gnome-games_42+3+11.0trisquel3_all.deb
8d6076728d6b509e41ab7fa174a3084f309d06b0ab51c37d6bd82f6c7e1d1c7d  ./gnome_42+3+11.0trisquel3_amd64.deb
8adb67d71d943e9fc48b576eee04ab63af967b12e2316da0010565f726d3d7ce  ./gnome-api-docs_42+3+11.0trisquel3_all.deb
+ cd ..
+ sha256sum -c SHA256SUMS
./gnome-core_42+3+11.0trisquel3_amd64.deb: OK
./gnome-devel_42+3+11.0trisquel3_all.deb: OK
./gnome-platform-devel_42+3+11.0trisquel3_all.deb: OK
./gnome-games_42+3+11.0trisquel3_all.deb: OK
./gnome_42+3+11.0trisquel3_amd64.deb: OK
./gnome-api-docs_42+3+11.0trisquel3_all.deb: OK
+ echo Package meta-gnome3 version 1:42+3+11.0trisquel3 is reproducible!
Package meta-gnome3 version 1:42+3+11.0trisquel3 is reproducible!
