+ date
Sun Apr  2 08:12:35 UTC 2023
+ apt-get source --only-source sugar-datastore
Reading package lists...
NOTICE: 'sugar-datastore' packaging is maintained in the 'Git' version control system at:
https://salsa.debian.org/pkg-sugar-team/sugar-datastore.git
Please use:
git clone https://salsa.debian.org/pkg-sugar-team/sugar-datastore.git
to retrieve the latest (possibly unreleased) updates to the package.
Need to get 252 kB of source archives.
Get:1 http://archive.trisquel.info/trisquel aramo/main sugar-datastore 0.119-1+11.0trisquel1 (dsc) [1881 B]
Get:2 http://archive.trisquel.info/trisquel aramo/main sugar-datastore 0.119-1+11.0trisquel1 (tar) [251 kB]
dpkg-source: info: extracting sugar-datastore in sugar-datastore-0.119
dpkg-source: info: unpacking sugar-datastore_0.119-1+11.0trisquel1.tar.xz
Fetched 252 kB in 1s (356 kB/s)
W: Download is performed unsandboxed as root as file 'sugar-datastore_0.119-1+11.0trisquel1.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ apt-get build-dep -y --only-source sugar-datastore
Reading package lists...
Building dependency tree...
Reading state information...
The following NEW packages will be installed:
  autoconf automake autopoint autotools-dev debhelper debugedit dh-autoreconf
  dh-python dh-strip-nondeterminism dwz gobject-introspection
  libdebhelper-perl libfile-stripnondeterminism-perl libsub-override-perl
  libtool m4 po-debconf python3-importlib-metadata python3-mako
  python3-markdown python3-markupsafe python3-more-itertools python3-zipp
0 upgraded, 23 newly installed, 0 to remove and 1 not upgraded.
Need to get 3764 kB of archives.
After this operation, 12.0 MB of additional disk space will be used.
Get:1 http://archive.trisquel.info/trisquel aramo/main amd64 m4 amd64 1.4.18-5ubuntu2 [199 kB]
Get:2 http://archive.trisquel.info/trisquel aramo/main amd64 autoconf all 2.71-2 [338 kB]
Get:3 http://archive.trisquel.info/trisquel aramo/main amd64 autotools-dev all 20220109.1 [44.9 kB]
Get:4 http://archive.trisquel.info/trisquel aramo/main amd64 automake all 1:1.16.5-1.3 [558 kB]
Get:5 http://archive.trisquel.info/trisquel aramo/main amd64 autopoint all 0.21-4ubuntu4 [422 kB]
Get:6 http://archive.trisquel.info/trisquel aramo/main amd64 libdebhelper-perl all 13.6ubuntu1 [67.2 kB]
Get:7 http://archive.trisquel.info/trisquel aramo/main amd64 libtool all 2.4.6-15build2 [164 kB]
Get:8 http://archive.trisquel.info/trisquel aramo/main amd64 dh-autoreconf all 20 [16.1 kB]
Get:9 http://archive.trisquel.info/trisquel aramo/main amd64 libsub-override-perl all 0.09-2 [9532 B]
Get:10 http://archive.trisquel.info/trisquel aramo/main amd64 libfile-stripnondeterminism-perl all 1.13.0-1 [18.1 kB]
Get:11 http://archive.trisquel.info/trisquel aramo/main amd64 dh-strip-nondeterminism all 1.13.0-1 [5344 B]
Get:12 http://archive.trisquel.info/trisquel aramo/main amd64 debugedit amd64 1:5.0-4build1 [47.2 kB]
Get:13 http://archive.trisquel.info/trisquel aramo/main amd64 dwz amd64 0.14-1build2 [105 kB]
Get:14 http://archive.trisquel.info/trisquel aramo/main amd64 po-debconf all 1.0.21+nmu1 [233 kB]
Get:15 http://archive.trisquel.info/trisquel aramo/main amd64 debhelper all 13.6ubuntu1 [923 kB]
Get:16 http://archive.trisquel.info/trisquel aramo/main amd64 dh-python all 5.20220403 [106 kB]
Get:17 http://archive.trisquel.info/trisquel aramo/main amd64 python3-markupsafe amd64 2.0.1-2build1 [12.7 kB]
Get:18 http://archive.trisquel.info/trisquel aramo-updates/main amd64 python3-mako all 1.1.3+ds1-2ubuntu0.1 [60.5 kB]
Get:19 http://archive.trisquel.info/trisquel aramo/main amd64 python3-more-itertools all 8.10.0-2 [47.9 kB]
Get:20 http://archive.trisquel.info/trisquel aramo/main amd64 python3-zipp all 1.0.0-3 [5440 B]
Get:21 http://archive.trisquel.info/trisquel aramo/main amd64 python3-importlib-metadata all 4.6.4-1 [16.2 kB]
Get:22 http://archive.trisquel.info/trisquel aramo/main amd64 python3-markdown all 3.3.6-1 [68.5 kB]
Get:23 http://archive.trisquel.info/trisquel aramo/main amd64 gobject-introspection amd64 1.72.0-1 [297 kB]
debconf: delaying package configuration, since apt-utils is not installed
Fetched 3764 kB in 1s (3585 kB/s)
Selecting previously unselected package m4.
(Reading database ... (Reading database ... 5%(Reading database ... 10%(Reading database ... 15%(Reading database ... 20%(Reading database ... 25%(Reading database ... 30%(Reading database ... 35%(Reading database ... 40%(Reading database ... 45%(Reading database ... 50%(Reading database ... 55%(Reading database ... 60%(Reading database ... 65%(Reading database ... 70%(Reading database ... 75%(Reading database ... 80%(Reading database ... 85%(Reading database ... 90%(Reading database ... 95%(Reading database ... 100%(Reading database ... 123231 files and directories currently installed.)
Preparing to unpack .../00-m4_1.4.18-5ubuntu2_amd64.deb ...
Unpacking m4 (1.4.18-5ubuntu2) ...
Selecting previously unselected package autoconf.
Preparing to unpack .../01-autoconf_2.71-2_all.deb ...
Unpacking autoconf (2.71-2) ...
Selecting previously unselected package autotools-dev.
Preparing to unpack .../02-autotools-dev_20220109.1_all.deb ...
Unpacking autotools-dev (20220109.1) ...
Selecting previously unselected package automake.
Preparing to unpack .../03-automake_1%3a1.16.5-1.3_all.deb ...
Unpacking automake (1:1.16.5-1.3) ...
Selecting previously unselected package autopoint.
Preparing to unpack .../04-autopoint_0.21-4ubuntu4_all.deb ...
Unpacking autopoint (0.21-4ubuntu4) ...
Selecting previously unselected package libdebhelper-perl.
Preparing to unpack .../05-libdebhelper-perl_13.6ubuntu1_all.deb ...
Unpacking libdebhelper-perl (13.6ubuntu1) ...
Selecting previously unselected package libtool.
Preparing to unpack .../06-libtool_2.4.6-15build2_all.deb ...
Unpacking libtool (2.4.6-15build2) ...
Selecting previously unselected package dh-autoreconf.
Preparing to unpack .../07-dh-autoreconf_20_all.deb ...
Unpacking dh-autoreconf (20) ...
Selecting previously unselected package libsub-override-perl.
Preparing to unpack .../08-libsub-override-perl_0.09-2_all.deb ...
Unpacking libsub-override-perl (0.09-2) ...
Selecting previously unselected package libfile-stripnondeterminism-perl.
Preparing to unpack .../09-libfile-stripnondeterminism-perl_1.13.0-1_all.deb ...
Unpacking libfile-stripnondeterminism-perl (1.13.0-1) ...
Selecting previously unselected package dh-strip-nondeterminism.
Preparing to unpack .../10-dh-strip-nondeterminism_1.13.0-1_all.deb ...
Unpacking dh-strip-nondeterminism (1.13.0-1) ...
Selecting previously unselected package debugedit.
Preparing to unpack .../11-debugedit_1%3a5.0-4build1_amd64.deb ...
Unpacking debugedit (1:5.0-4build1) ...
Selecting previously unselected package dwz.
Preparing to unpack .../12-dwz_0.14-1build2_amd64.deb ...
Unpacking dwz (0.14-1build2) ...
Selecting previously unselected package po-debconf.
Preparing to unpack .../13-po-debconf_1.0.21+nmu1_all.deb ...
Unpacking po-debconf (1.0.21+nmu1) ...
Selecting previously unselected package debhelper.
Preparing to unpack .../14-debhelper_13.6ubuntu1_all.deb ...
Unpacking debhelper (13.6ubuntu1) ...
Selecting previously unselected package dh-python.
Preparing to unpack .../15-dh-python_5.20220403_all.deb ...
Unpacking dh-python (5.20220403) ...
Selecting previously unselected package python3-markupsafe.
Preparing to unpack .../16-python3-markupsafe_2.0.1-2build1_amd64.deb ...
Unpacking python3-markupsafe (2.0.1-2build1) ...
Selecting previously unselected package python3-mako.
Preparing to unpack .../17-python3-mako_1.1.3+ds1-2ubuntu0.1_all.deb ...
Unpacking python3-mako (1.1.3+ds1-2ubuntu0.1) ...
Selecting previously unselected package python3-more-itertools.
Preparing to unpack .../18-python3-more-itertools_8.10.0-2_all.deb ...
Unpacking python3-more-itertools (8.10.0-2) ...
Selecting previously unselected package python3-zipp.
Preparing to unpack .../19-python3-zipp_1.0.0-3_all.deb ...
Unpacking python3-zipp (1.0.0-3) ...
Selecting previously unselected package python3-importlib-metadata.
Preparing to unpack .../20-python3-importlib-metadata_4.6.4-1_all.deb ...
Unpacking python3-importlib-metadata (4.6.4-1) ...
Selecting previously unselected package python3-markdown.
Preparing to unpack .../21-python3-markdown_3.3.6-1_all.deb ...
Unpacking python3-markdown (3.3.6-1) ...
Selecting previously unselected package gobject-introspection.
Preparing to unpack .../22-gobject-introspection_1.72.0-1_amd64.deb ...
Unpacking gobject-introspection (1.72.0-1) ...
Setting up dh-python (5.20220403) ...
Setting up python3-more-itertools (8.10.0-2) ...
Setting up po-debconf (1.0.21+nmu1) ...
Setting up libdebhelper-perl (13.6ubuntu1) ...
Setting up m4 (1.4.18-5ubuntu2) ...
Setting up python3-zipp (1.0.0-3) ...
Setting up python3-markupsafe (2.0.1-2build1) ...
Setting up autotools-dev (20220109.1) ...
Setting up autopoint (0.21-4ubuntu4) ...
Setting up autoconf (2.71-2) ...
Setting up dwz (0.14-1build2) ...
Setting up debugedit (1:5.0-4build1) ...
Setting up libsub-override-perl (0.09-2) ...
Setting up python3-mako (1.1.3+ds1-2ubuntu0.1) ...
Setting up automake (1:1.16.5-1.3) ...
update-alternatives: using /usr/bin/automake-1.16 to provide /usr/bin/automake (automake) in auto mode
Setting up libfile-stripnondeterminism-perl (1.13.0-1) ...
Setting up python3-importlib-metadata (4.6.4-1) ...
Setting up libtool (2.4.6-15build2) ...
Setting up dh-autoreconf (20) ...
Setting up python3-markdown (3.3.6-1) ...
Setting up dh-strip-nondeterminism (1.13.0-1) ...
Setting up gobject-introspection (1.72.0-1) ...
Setting up debhelper (13.6ubuntu1) ...
Processing triggers for man-db (2.10.2-1) ...
+ find . -maxdepth 1 -name sugar-datastore* -type d
+ cd ./sugar-datastore-0.119
+ eatmydata env DEB_BUILD_OPTIONS=noautodbgsym dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package sugar-datastore
dpkg-buildpackage: info: source version 0.119-1+11.0trisquel1
dpkg-buildpackage: info: source distribution aramo
dpkg-buildpackage: info: source changed by Trisquel GNU/Linux developers <trisquel-devel@listas.trisquel.info>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 debian/rules clean
dh "clean"
   dh_clean
 dpkg-source -b .
dpkg-source: warning: native package version may not have a revision
dpkg-source: info: using source format '3.0 (native)'
dpkg-source: info: building sugar-datastore in sugar-datastore_0.119-1+11.0trisquel1.tar.xz
dpkg-source: info: building sugar-datastore in sugar-datastore_0.119-1+11.0trisquel1.dsc
 debian/rules binary
dh "binary"
   dh_update_autotools_config
   dh_autoreconf
libtoolize: putting auxiliary files in '.'.
libtoolize: copying file './ltmain.sh'
libtoolize: putting macros in AC_CONFIG_MACRO_DIRS, 'm4'.
libtoolize: copying file 'm4/libtool.m4'
libtoolize: copying file 'm4/ltoptions.m4'
libtoolize: copying file 'm4/ltsugar.m4'
libtoolize: copying file 'm4/ltversion.m4'
libtoolize: copying file 'm4/lt~obsolete.m4'
configure.ac:13: warning: The macro `AC_PROG_LIBTOOL' is obsolete.
configure.ac:13: You should run autoupdate.
m4/libtool.m4:99: AC_PROG_LIBTOOL is expanded from...
configure.ac:13: the top level
configure.ac:17: warning: The macro `AC_TRY_CPP' is obsolete.
configure.ac:17: You should run autoupdate.
./lib/autoconf/general.m4:2762: AC_TRY_CPP is expanded from...
m4/python.m4:42: AM_CHECK_PYTHON_HEADERS is expanded from...
configure.ac:17: the top level
configure.ac:19: warning: AC_OUTPUT should be used without arguments.
configure.ac:19: You should run autoupdate.
configure.ac:13: installing './compile'
configure.ac:8: installing './missing'
src/carquinyol/Makefile.am: installing './depcomp'
   dh_auto_configure
	./configure --build=x86_64-linux-gnu --prefix=/usr --includedir=\${prefix}/include --mandir=\${prefix}/share/man --infodir=\${prefix}/share/info --sysconfdir=/etc --localstatedir=/var --disable-option-checking --disable-silent-rules --libdir=\${prefix}/lib/x86_64-linux-gnu --runstatedir=/run --disable-maintainer-mode --disable-dependency-tracking
checking for a BSD-compatible install... /usr/bin/install -c
checking whether build environment is sane... yes
checking for a race-free mkdir -p... /usr/bin/mkdir -p
checking for gawk... gawk
checking whether make sets $(MAKE)... yes
checking whether make supports nested variables... yes
checking whether to enable maintainer-specific portions of Makefiles... no
checking build system type... x86_64-pc-linux-gnu
checking host system type... x86_64-pc-linux-gnu
checking how to print strings... printf
checking whether make supports the include directive... yes (GNU style)
checking for gcc... gcc
checking whether the C compiler works... yes
checking for C compiler default output file name... a.out
checking for suffix of executables... 
checking whether we are cross compiling... no
checking for suffix of object files... o
checking whether the compiler supports GNU C... yes
checking whether gcc accepts -g... yes
checking for gcc option to enable C11 features... none needed
checking whether gcc understands -c and -o together... yes
checking dependency style of gcc... none
checking for a sed that does not truncate output... /usr/bin/sed
checking for grep that handles long lines and -e... /usr/bin/grep
checking for egrep... /usr/bin/grep -E
checking for fgrep... /usr/bin/grep -F
checking for ld used by gcc... /usr/bin/ld
checking if the linker (/usr/bin/ld) is GNU ld... yes
checking for BSD- or MS-compatible name lister (nm)... /usr/bin/nm -B
checking the name lister (/usr/bin/nm -B) interface... BSD nm
checking whether ln -s works... yes
checking the maximum length of command line arguments... 1572864
checking how to convert x86_64-pc-linux-gnu file names to x86_64-pc-linux-gnu format... func_convert_file_noop
checking how to convert x86_64-pc-linux-gnu file names to toolchain format... func_convert_file_noop
checking for /usr/bin/ld option to reload object files... -r
checking for objdump... objdump
checking how to recognize dependent libraries... pass_all
checking for dlltool... no
checking how to associate runtime and link libraries... printf %s\n
checking for ar... ar
checking for archiver @FILE support... @
checking for strip... strip
checking for ranlib... ranlib
checking command to parse /usr/bin/nm -B output from gcc object... ok
checking for sysroot... no
checking for a working dd... /usr/bin/dd
checking how to truncate binary pipes... /usr/bin/dd bs=4096 count=1
checking for mt... mt
checking if mt is a manifest tool... no
checking for stdio.h... yes
checking for stdlib.h... yes
checking for string.h... yes
checking for inttypes.h... yes
checking for stdint.h... yes
checking for strings.h... yes
checking for sys/stat.h... yes
checking for sys/types.h... yes
checking for unistd.h... yes
checking for dlfcn.h... yes
checking for objdir... .libs
checking if gcc supports -fno-rtti -fno-exceptions... no
checking for gcc option to produce PIC... -fPIC -DPIC
checking if gcc PIC flag -fPIC -DPIC works... yes
checking if gcc static flag -static works... yes
checking if gcc supports -c -o file.o... yes
checking if gcc supports -c -o file.o... (cached) yes
checking whether the gcc linker (/usr/bin/ld -m elf_x86_64) supports shared libraries... yes
checking whether -lc should be explicitly linked in... no
checking dynamic linker characteristics... GNU/Linux ld.so
checking how to hardcode library paths into programs... immediate
checking whether stripping libraries is possible... yes
checking if libtool supports shared libraries... yes
checking whether to build shared libraries... yes
checking whether to build static libraries... no
checking whether python3 version is >= 3... yes
checking for python3 version... 3.10
checking for python3 platform... linux
checking for GNU default python3 prefix... ${prefix}
checking for GNU default python3 exec_prefix... ${exec_prefix}
checking for python3 script directory (pythondir)... ${PYTHON_PREFIX}/lib/python3.10/site-packages
checking for python3 extension module directory (pyexecdir)... ${PYTHON_EXEC_PREFIX}/lib/python3.10/site-packages
checking how to run the C preprocessor... gcc -E
checking for headers required to compile python extensions... found
checking that generated files are newer than configure... done
configure: creating ./config.status
config.status: creating Makefile
config.status: creating bin/Makefile
config.status: creating etc/Makefile
config.status: creating src/Makefile
config.status: creating src/carquinyol/Makefile
config.status: executing depfiles commands
config.status: executing libtool commands
   dh_auto_build
	make -j1
make[1]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/sugar-datastore/sugar-datastore-0.119'
Making all in bin
make[2]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/sugar-datastore/sugar-datastore-0.119/bin'
make[2]: Nothing to be done for 'all'.
make[2]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/sugar-datastore/sugar-datastore-0.119/bin'
Making all in etc
make[2]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/sugar-datastore/sugar-datastore-0.119/etc'
make[2]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/sugar-datastore/sugar-datastore-0.119/etc'
Making all in src
make[2]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/sugar-datastore/sugar-datastore-0.119/src'
Making all in carquinyol
make[3]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/sugar-datastore/sugar-datastore-0.119/src/carquinyol'
/bin/bash ../../libtool  --tag=CC   --mode=compile gcc -DPACKAGE_NAME=\"sugar-datastore\" -DPACKAGE_TARNAME=\"sugar-datastore\" -DPACKAGE_VERSION=\"0.119\" -DPACKAGE_STRING=\"sugar-datastore\ 0.119\" -DPACKAGE_BUGREPORT=\"\" -DPACKAGE_URL=\"\" -DPACKAGE=\"sugar-datastore\" -DVERSION=\"0.119\" -DHAVE_STDIO_H=1 -DHAVE_STDLIB_H=1 -DHAVE_STRING_H=1 -DHAVE_INTTYPES_H=1 -DHAVE_STDINT_H=1 -DHAVE_STRINGS_H=1 -DHAVE_SYS_STAT_H=1 -DHAVE_SYS_TYPES_H=1 -DHAVE_UNISTD_H=1 -DSTDC_HEADERS=1 -DHAVE_DLFCN_H=1 -DLT_OBJDIR=\".libs/\" -I.    -I/usr/include/python3.10 -Wdate-time -D_FORTIFY_SOURCE=2  -g -O2 -ffile-prefix-map=/builds/debdistutils/reproduce-trisquel/buildlogs/sugar-datastore/sugar-datastore-0.119=. -flto=auto -ffat-lto-objects -flto=auto -ffat-lto-objects -fstack-protector-strong -Wformat -Werror=format-security -c -o metadatareader.lo metadatareader.c
libtool: compile:  gcc -DPACKAGE_NAME=\"sugar-datastore\" -DPACKAGE_TARNAME=\"sugar-datastore\" -DPACKAGE_VERSION=\"0.119\" "-DPACKAGE_STRING=\"sugar-datastore 0.119\"" -DPACKAGE_BUGREPORT=\"\" -DPACKAGE_URL=\"\" -DPACKAGE=\"sugar-datastore\" -DVERSION=\"0.119\" -DHAVE_STDIO_H=1 -DHAVE_STDLIB_H=1 -DHAVE_STRING_H=1 -DHAVE_INTTYPES_H=1 -DHAVE_STDINT_H=1 -DHAVE_STRINGS_H=1 -DHAVE_SYS_STAT_H=1 -DHAVE_SYS_TYPES_H=1 -DHAVE_UNISTD_H=1 -DSTDC_HEADERS=1 -DHAVE_DLFCN_H=1 -DLT_OBJDIR=\".libs/\" -I. -I/usr/include/python3.10 -Wdate-time -D_FORTIFY_SOURCE=2 -g -O2 -ffile-prefix-map=/builds/debdistutils/reproduce-trisquel/buildlogs/sugar-datastore/sugar-datastore-0.119=. -flto=auto -ffat-lto-objects -flto=auto -ffat-lto-objects -fstack-protector-strong -Wformat -Werror=format-security -c metadatareader.c  -fPIC -DPIC -o .libs/metadatareader.o
/bin/bash ../../libtool  --tag=CC   --mode=link gcc  -g -O2 -ffile-prefix-map=/builds/debdistutils/reproduce-trisquel/buildlogs/sugar-datastore/sugar-datastore-0.119=. -flto=auto -ffat-lto-objects -flto=auto -ffat-lto-objects -fstack-protector-strong -Wformat -Werror=format-security -module -avoid-version -Wl,-Bsymbolic-functions -flto=auto -ffat-lto-objects -flto=auto -Wl,-z,relro -o metadatareader.la -rpath /usr/lib/python3.10/site-packages/carquinyol metadatareader.lo  
libtool: link: gcc -shared  -fPIC -DPIC  .libs/metadatareader.o    -g -O2 -flto=auto -flto=auto -fstack-protector-strong -Wl,-Bsymbolic-functions -flto=auto -flto=auto -Wl,-z -Wl,relro   -Wl,-soname -Wl,metadatareader.so -o .libs/metadatareader.so
libtool: link: ( cd ".libs" && rm -f "metadatareader.la" && ln -s "../metadatareader.la" "metadatareader.la" )
make[3]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/sugar-datastore/sugar-datastore-0.119/src/carquinyol'
make[3]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/sugar-datastore/sugar-datastore-0.119/src'
make[3]: Nothing to be done for 'all-am'.
make[3]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/sugar-datastore/sugar-datastore-0.119/src'
make[2]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/sugar-datastore/sugar-datastore-0.119/src'
make[2]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/sugar-datastore/sugar-datastore-0.119'
make[2]: Nothing to be done for 'all-am'.
make[2]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/sugar-datastore/sugar-datastore-0.119'
make[1]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/sugar-datastore/sugar-datastore-0.119'
   dh_auto_test
	make -j1 check VERBOSE=1
make[1]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/sugar-datastore/sugar-datastore-0.119'
Making check in bin
make[2]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/sugar-datastore/sugar-datastore-0.119/bin'
make[2]: Nothing to be done for 'check'.
make[2]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/sugar-datastore/sugar-datastore-0.119/bin'
Making check in etc
make[2]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/sugar-datastore/sugar-datastore-0.119/etc'
make[2]: Nothing to be done for 'check'.
make[2]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/sugar-datastore/sugar-datastore-0.119/etc'
Making check in src
make[2]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/sugar-datastore/sugar-datastore-0.119/src'
Making check in carquinyol
make[3]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/sugar-datastore/sugar-datastore-0.119/src/carquinyol'
make[3]: Nothing to be done for 'check'.
make[3]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/sugar-datastore/sugar-datastore-0.119/src/carquinyol'
make[3]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/sugar-datastore/sugar-datastore-0.119/src'
make[3]: Nothing to be done for 'check-am'.
make[3]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/sugar-datastore/sugar-datastore-0.119/src'
make[2]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/sugar-datastore/sugar-datastore-0.119/src'
make[2]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/sugar-datastore/sugar-datastore-0.119'
make[2]: Nothing to be done for 'check-am'.
make[2]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/sugar-datastore/sugar-datastore-0.119'
make[1]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/sugar-datastore/sugar-datastore-0.119'
   create-stamp debian/debhelper-build-stamp
   dh_prep
   dh_auto_install --destdir=debian/python3-carquinyol/
	make -j1 install DESTDIR=/builds/debdistutils/reproduce-trisquel/buildlogs/sugar-datastore/sugar-datastore-0.119/debian/python3-carquinyol AM_UPDATE_INFO_DIR=no
make[1]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/sugar-datastore/sugar-datastore-0.119'
Making install in bin
make[2]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/sugar-datastore/sugar-datastore-0.119/bin'
make[3]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/sugar-datastore/sugar-datastore-0.119/bin'
 /usr/bin/mkdir -p '/builds/debdistutils/reproduce-trisquel/buildlogs/sugar-datastore/sugar-datastore-0.119/debian/python3-carquinyol/usr/bin'
 /usr/bin/install -c datastore-service copy-from-journal copy-to-journal '/builds/debdistutils/reproduce-trisquel/buildlogs/sugar-datastore/sugar-datastore-0.119/debian/python3-carquinyol/usr/bin'
make[3]: Nothing to be done for 'install-data-am'.
make[3]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/sugar-datastore/sugar-datastore-0.119/bin'
make[2]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/sugar-datastore/sugar-datastore-0.119/bin'
Making install in etc
make[2]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/sugar-datastore/sugar-datastore-0.119/etc'
make[3]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/sugar-datastore/sugar-datastore-0.119/etc'
make[3]: Nothing to be done for 'install-exec-am'.
 /usr/bin/mkdir -p '/builds/debdistutils/reproduce-trisquel/buildlogs/sugar-datastore/sugar-datastore-0.119/debian/python3-carquinyol/usr/share/dbus-1/services'
 /usr/bin/install -c -m 644 org.laptop.sugar.DataStore.service '/builds/debdistutils/reproduce-trisquel/buildlogs/sugar-datastore/sugar-datastore-0.119/debian/python3-carquinyol/usr/share/dbus-1/services'
make[3]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/sugar-datastore/sugar-datastore-0.119/etc'
make[2]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/sugar-datastore/sugar-datastore-0.119/etc'
Making install in src
make[2]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/sugar-datastore/sugar-datastore-0.119/src'
Making install in carquinyol
make[3]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/sugar-datastore/sugar-datastore-0.119/src/carquinyol'
make[4]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/sugar-datastore/sugar-datastore-0.119/src/carquinyol'
 /usr/bin/mkdir -p '/builds/debdistutils/reproduce-trisquel/buildlogs/sugar-datastore/sugar-datastore-0.119/debian/python3-carquinyol/usr/lib/python3.10/site-packages/carquinyol'
 /bin/bash ../../libtool   --mode=install /usr/bin/install -c   metadatareader.la '/builds/debdistutils/reproduce-trisquel/buildlogs/sugar-datastore/sugar-datastore-0.119/debian/python3-carquinyol/usr/lib/python3.10/site-packages/carquinyol'
libtool: install: /usr/bin/install -c .libs/metadatareader.so /builds/debdistutils/reproduce-trisquel/buildlogs/sugar-datastore/sugar-datastore-0.119/debian/python3-carquinyol/usr/lib/python3.10/site-packages/carquinyol/metadatareader.so
libtool: install: /usr/bin/install -c .libs/metadatareader.lai /builds/debdistutils/reproduce-trisquel/buildlogs/sugar-datastore/sugar-datastore-0.119/debian/python3-carquinyol/usr/lib/python3.10/site-packages/carquinyol/metadatareader.la
libtool: warning: remember to run 'libtool --finish /usr/lib/python3.10/site-packages/carquinyol'
 /usr/bin/mkdir -p '/builds/debdistutils/reproduce-trisquel/buildlogs/sugar-datastore/sugar-datastore-0.119/debian/python3-carquinyol/usr/lib/python3.10/site-packages/carquinyol'
 /usr/bin/install -c -m 644 __init__.py datastore.py filestore.py indexstore.py layoutmanager.py metadatastore.py migration.py optimizer.py '/builds/debdistutils/reproduce-trisquel/buildlogs/sugar-datastore/sugar-datastore-0.119/debian/python3-carquinyol/usr/lib/python3.10/site-packages/carquinyol'
Byte-compiling python modules...
__init__.pydatastore.pyfilestore.pyindexstore.pylayoutmanager.pymetadatastore.pymigration.pyoptimizer.py
Byte-compiling python modules (optimized versions) ...
__init__.pydatastore.pyfilestore.pyindexstore.pylayoutmanager.pymetadatastore.pymigration.pyoptimizer.py
make[4]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/sugar-datastore/sugar-datastore-0.119/src/carquinyol'
make[3]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/sugar-datastore/sugar-datastore-0.119/src/carquinyol'
make[3]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/sugar-datastore/sugar-datastore-0.119/src'
make[4]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/sugar-datastore/sugar-datastore-0.119/src'
make[4]: Nothing to be done for 'install-exec-am'.
make[4]: Nothing to be done for 'install-data-am'.
make[4]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/sugar-datastore/sugar-datastore-0.119/src'
make[3]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/sugar-datastore/sugar-datastore-0.119/src'
make[2]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/sugar-datastore/sugar-datastore-0.119/src'
make[2]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/sugar-datastore/sugar-datastore-0.119'
make[3]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/sugar-datastore/sugar-datastore-0.119'
make[3]: Nothing to be done for 'install-exec-am'.
make[3]: Nothing to be done for 'install-data-am'.
make[3]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/sugar-datastore/sugar-datastore-0.119'
make[2]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/sugar-datastore/sugar-datastore-0.119'
make[1]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/sugar-datastore/sugar-datastore-0.119'
   debian/rules execute_after_dh_install
make[1]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/sugar-datastore/sugar-datastore-0.119'
find debian/*/usr/lib -name '*.la' -delete
make[1]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/sugar-datastore/sugar-datastore-0.119'
   dh_installdocs
   dh_installchangelogs
   dh_python3
I: dh_python3 fs:482: renaming metadatareader.so to metadatareader.cpython-310-x86_64-linux-gnu.so
I: dh_python3 tools:114: replacing shebang in debian/python3-carquinyol/usr/bin/copy-from-journal
I: dh_python3 tools:114: replacing shebang in debian/python3-carquinyol/usr/bin/datastore-service
I: dh_python3 tools:114: replacing shebang in debian/python3-carquinyol/usr/bin/copy-to-journal
   dh_perl
   dh_link
   dh_strip_nondeterminism
   dh_compress
   dh_fixperms
   dh_missing
   dh_dwz -a
   dh_strip -a
b14760b63d44fc4b19820240c755a6bce9e9d860
   dh_makeshlibs -a
   dh_shlibdeps -a
dpkg-shlibdeps: warning: debian/python3-carquinyol/usr/lib/python3/dist-packages/carquinyol/metadatareader.cpython-310-x86_64-linux-gnu.so contains an unresolvable reference to symbol _Py_BuildValue_SizeT: it's probably a plugin
dpkg-shlibdeps: warning: 19 other similar warnings have been skipped (use -v to see them all)
   dh_girepository
   dh_installdeb
   dh_gencontrol
dpkg-gencontrol: warning: package python3-carquinyol: substitution variable ${python3:Versions} unused, but is defined
   dh_md5sums
   dh_builddeb
pkgstripfiles: processing control file: debian/python3-carquinyol/DEBIAN/control, package python3-carquinyol, directory debian/python3-carquinyol
pkgstripfiles: Truncating usr/share/doc/python3-carquinyol/changelog.Debian.gz to topmost ten records
pkgstripfiles: Running PNG optimization (using 1 cpus) for package python3-carquinyol ...
pkgstripfiles: No PNG files.
dpkg-deb: building package 'python3-carquinyol' in '../python3-carquinyol_0.119-1+11.0trisquel1_amd64.deb'.
 dpkg-genbuildinfo -O../sugar-datastore_0.119-1+11.0trisquel1_amd64.buildinfo
 dpkg-genchanges -O../sugar-datastore_0.119-1+11.0trisquel1_amd64.changes
dpkg-genchanges: info: including full source code in upload
 dpkg-source --after-build .
dpkg-buildpackage: info: full upload; Debian-native package (full source is included)
+ cd ..
+ ls -la
total 332
drwxr-xr-x 3 root root   4096 Apr  2 08:12 .
drwxr-xr-x 3 root root   4096 Apr  2 08:12 ..
-rw-r--r-- 1 root root  31608 Apr  2 08:12 buildlog.txt
-rw-r--r-- 1 root root  24090 Apr  2 08:12 python3-carquinyol_0.119-1+11.0trisquel1_amd64.deb
drwxr-xr-x 8 root root   4096 Apr  2 08:12 sugar-datastore-0.119
-rw-r--r-- 1 root root    998 Apr  2 08:12 sugar-datastore_0.119-1+11.0trisquel1.dsc
-rw-r--r-- 1 root root 250516 Apr  2 08:12 sugar-datastore_0.119-1+11.0trisquel1.tar.xz
-rw-r--r-- 1 root root   7994 Apr  2 08:12 sugar-datastore_0.119-1+11.0trisquel1_amd64.buildinfo
-rw-r--r-- 1 root root   1845 Apr  2 08:12 sugar-datastore_0.119-1+11.0trisquel1_amd64.changes
+ find . -maxdepth 1 -type f
+ sha256sum ./sugar-datastore_0.119-1+11.0trisquel1_amd64.buildinfo ./sugar-datastore_0.119-1+11.0trisquel1.tar.xz ./buildlog.txt ./sugar-datastore_0.119-1+11.0trisquel1_amd64.changes ./sugar-datastore_0.119-1+11.0trisquel1.dsc ./python3-carquinyol_0.119-1+11.0trisquel1_amd64.deb
2522bbcc9770cf4380a5eaf00631c519fa0ddeb1a1c68548a0bb05262fab16de  ./sugar-datastore_0.119-1+11.0trisquel1_amd64.buildinfo
6d5f708cf24524af5515a8731adf88a72831dc18c43e22cff2dc6cb42182e2b7  ./sugar-datastore_0.119-1+11.0trisquel1.tar.xz
03639b3b8d5ecf06661cb9bb3f2e6fb38b030103acbeff712c5e40dd2fe346de  ./buildlog.txt
f3c620561dcbe06b002c980453f10b42db4c9640c5763ebfdcdf89169e791ef0  ./sugar-datastore_0.119-1+11.0trisquel1_amd64.changes
fbc37e904bb327054deab563753eaaf47aba08d3c09ca63fd721a0da44b6115f  ./sugar-datastore_0.119-1+11.0trisquel1.dsc
176a00500704e8b92a14a28e08b65eec0815e7a2f9a62c0d4d2cad4e92f82053  ./python3-carquinyol_0.119-1+11.0trisquel1_amd64.deb
+ mkdir published
+ cd published
+ sed -e s,.*/\([^_]*\)_.*,\1,
+ echo ../python3-carquinyol_0.119-1+11.0trisquel1_amd64.deb
+ deb=python3-carquinyol
+ cut -d' -f2
+ grep ^'
+ apt-get --print-uris --yes download python3-carquinyol
+ url=http://archive.trisquel.info/trisquel/pool/main/s/sugar-datastore/python3-carquinyol_0.119-1%2b11.0trisquel1_amd64.deb
+ wget -q http://archive.trisquel.info/trisquel/pool/main/s/sugar-datastore/python3-carquinyol_0.119-1%2b11.0trisquel1_amd64.deb
+ tee ../SHA256SUMS
+ find . -maxdepth 1 -type f
+ sha256sum ./python3-carquinyol_0.119-1+11.0trisquel1_amd64.deb
2ed56d6341689fc4b1e54c5e391320693eeba46d7c900fb2ce8da9de76d3bdae  ./python3-carquinyol_0.119-1+11.0trisquel1_amd64.deb
+ cd ..
+ sha256sum -c SHA256SUMS
./python3-carquinyol_0.119-1+11.0trisquel1_amd64.deb: FAILED
sha256sum: WARNING: 1 computed checksum did NOT match
