+ date
Sun Apr  2 09:33:47 UTC 2023
+ apt-get source --only-source youtube-dl
Reading package lists...
NOTICE: 'youtube-dl' packaging is maintained in the 'Git' version control system at:
https://salsa.debian.org/debian/youtube-dl.git
Please use:
git clone https://salsa.debian.org/debian/youtube-dl.git
to retrieve the latest (possibly unreleased) updates to the package.
Need to get 2881 kB of source archives.
Get:1 http://archive.trisquel.info/trisquel aramo/main youtube-dl 2021.12.17-2+11.0trisquel3 (dsc) [1747 B]
Get:2 http://archive.trisquel.info/trisquel aramo/main youtube-dl 2021.12.17-2+11.0trisquel3 (tar) [2880 kB]
dpkg-source: info: extracting youtube-dl in youtube-dl-2021.12.17
dpkg-source: info: unpacking youtube-dl_2021.12.17-2+11.0trisquel3.tar.xz
Fetched 2881 kB in 1s (4500 kB/s)
W: Download is performed unsandboxed as root as file 'youtube-dl_2021.12.17-2+11.0trisquel3.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source youtube-dl
Reading package lists...
Building dependency tree...
Reading state information...
The following NEW packages will be installed:
  autoconf automake autopoint autotools-dev debhelper debugedit dh-autoreconf
  dh-strip-nondeterminism dwz libdebhelper-perl
  libfile-stripnondeterminism-perl libsub-override-perl libtool m4 po-debconf
0 upgraded, 15 newly installed, 0 to remove and 1 not upgraded.
Need to get 3149 kB of archives.
After this operation, 8859 kB of additional disk space will be used.
Get:1 http://archive.trisquel.info/trisquel aramo/main amd64 m4 amd64 1.4.18-5ubuntu2 [199 kB]
Get:2 http://archive.trisquel.info/trisquel aramo/main amd64 autoconf all 2.71-2 [338 kB]
Get:3 http://archive.trisquel.info/trisquel aramo/main amd64 autotools-dev all 20220109.1 [44.9 kB]
Get:4 http://archive.trisquel.info/trisquel aramo/main amd64 automake all 1:1.16.5-1.3 [558 kB]
Get:5 http://archive.trisquel.info/trisquel aramo/main amd64 autopoint all 0.21-4ubuntu4 [422 kB]
Get:6 http://archive.trisquel.info/trisquel aramo/main amd64 libdebhelper-perl all 13.6ubuntu1 [67.2 kB]
Get:7 http://archive.trisquel.info/trisquel aramo/main amd64 libtool all 2.4.6-15build2 [164 kB]
Get:8 http://archive.trisquel.info/trisquel aramo/main amd64 dh-autoreconf all 20 [16.1 kB]
Get:9 http://archive.trisquel.info/trisquel aramo/main amd64 libsub-override-perl all 0.09-2 [9532 B]
Get:10 http://archive.trisquel.info/trisquel aramo/main amd64 libfile-stripnondeterminism-perl all 1.13.0-1 [18.1 kB]
Get:11 http://archive.trisquel.info/trisquel aramo/main amd64 dh-strip-nondeterminism all 1.13.0-1 [5344 B]
Get:12 http://archive.trisquel.info/trisquel aramo/main amd64 debugedit amd64 1:5.0-4build1 [47.2 kB]
Get:13 http://archive.trisquel.info/trisquel aramo/main amd64 dwz amd64 0.14-1build2 [105 kB]
Get:14 http://archive.trisquel.info/trisquel aramo/main amd64 po-debconf all 1.0.21+nmu1 [233 kB]
Get:15 http://archive.trisquel.info/trisquel aramo/main amd64 debhelper all 13.6ubuntu1 [923 kB]
debconf: delaying package configuration, since apt-utils is not installed
Fetched 3149 kB in 0s (8923 kB/s)
Selecting previously unselected package m4.
(Reading database ... (Reading database ... 5%(Reading database ... 10%(Reading database ... 15%(Reading database ... 20%(Reading database ... 25%(Reading database ... 30%(Reading database ... 35%(Reading database ... 40%(Reading database ... 45%(Reading database ... 50%(Reading database ... 55%(Reading database ... 60%(Reading database ... 65%(Reading database ... 70%(Reading database ... 75%(Reading database ... 80%(Reading database ... 85%(Reading database ... 90%(Reading database ... 95%(Reading database ... 100%(Reading database ... 123231 files and directories currently installed.)
Preparing to unpack .../00-m4_1.4.18-5ubuntu2_amd64.deb ...
Unpacking m4 (1.4.18-5ubuntu2) ...
Selecting previously unselected package autoconf.
Preparing to unpack .../01-autoconf_2.71-2_all.deb ...
Unpacking autoconf (2.71-2) ...
Selecting previously unselected package autotools-dev.
Preparing to unpack .../02-autotools-dev_20220109.1_all.deb ...
Unpacking autotools-dev (20220109.1) ...
Selecting previously unselected package automake.
Preparing to unpack .../03-automake_1%3a1.16.5-1.3_all.deb ...
Unpacking automake (1:1.16.5-1.3) ...
Selecting previously unselected package autopoint.
Preparing to unpack .../04-autopoint_0.21-4ubuntu4_all.deb ...
Unpacking autopoint (0.21-4ubuntu4) ...
Selecting previously unselected package libdebhelper-perl.
Preparing to unpack .../05-libdebhelper-perl_13.6ubuntu1_all.deb ...
Unpacking libdebhelper-perl (13.6ubuntu1) ...
Selecting previously unselected package libtool.
Preparing to unpack .../06-libtool_2.4.6-15build2_all.deb ...
Unpacking libtool (2.4.6-15build2) ...
Selecting previously unselected package dh-autoreconf.
Preparing to unpack .../07-dh-autoreconf_20_all.deb ...
Unpacking dh-autoreconf (20) ...
Selecting previously unselected package libsub-override-perl.
Preparing to unpack .../08-libsub-override-perl_0.09-2_all.deb ...
Unpacking libsub-override-perl (0.09-2) ...
Selecting previously unselected package libfile-stripnondeterminism-perl.
Preparing to unpack .../09-libfile-stripnondeterminism-perl_1.13.0-1_all.deb ...
Unpacking libfile-stripnondeterminism-perl (1.13.0-1) ...
Selecting previously unselected package dh-strip-nondeterminism.
Preparing to unpack .../10-dh-strip-nondeterminism_1.13.0-1_all.deb ...
Unpacking dh-strip-nondeterminism (1.13.0-1) ...
Selecting previously unselected package debugedit.
Preparing to unpack .../11-debugedit_1%3a5.0-4build1_amd64.deb ...
Unpacking debugedit (1:5.0-4build1) ...
Selecting previously unselected package dwz.
Preparing to unpack .../12-dwz_0.14-1build2_amd64.deb ...
Unpacking dwz (0.14-1build2) ...
Selecting previously unselected package po-debconf.
Preparing to unpack .../13-po-debconf_1.0.21+nmu1_all.deb ...
Unpacking po-debconf (1.0.21+nmu1) ...
Selecting previously unselected package debhelper.
Preparing to unpack .../14-debhelper_13.6ubuntu1_all.deb ...
Unpacking debhelper (13.6ubuntu1) ...
Setting up po-debconf (1.0.21+nmu1) ...
Setting up libdebhelper-perl (13.6ubuntu1) ...
Setting up m4 (1.4.18-5ubuntu2) ...
Setting up autotools-dev (20220109.1) ...
Setting up autopoint (0.21-4ubuntu4) ...
Setting up autoconf (2.71-2) ...
Setting up dwz (0.14-1build2) ...
Setting up debugedit (1:5.0-4build1) ...
Setting up libsub-override-perl (0.09-2) ...
Setting up automake (1:1.16.5-1.3) ...
update-alternatives: using /usr/bin/automake-1.16 to provide /usr/bin/automake (automake) in auto mode
Setting up libfile-stripnondeterminism-perl (1.13.0-1) ...
Setting up libtool (2.4.6-15build2) ...
Setting up dh-autoreconf (20) ...
Setting up dh-strip-nondeterminism (1.13.0-1) ...
Setting up debhelper (13.6ubuntu1) ...
Processing triggers for man-db (2.10.2-1) ...
+ find . -maxdepth 1 -name youtube-dl* -type d
+ cd ./youtube-dl-2021.12.17
+ eatmydata env DEB_BUILD_OPTIONS=noautodbgsym dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package youtube-dl
dpkg-buildpackage: info: source version 2021.12.17-2+11.0trisquel3
dpkg-buildpackage: info: source distribution aramo
dpkg-buildpackage: info: source changed by Trisquel GNU/Linux developers <trisquel-devel@listas.trisquel.info>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 debian/rules clean
dh clean
   debian/rules override_dh_auto_clean
make[1]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/youtube-dl/youtube-dl-2021.12.17'

make[1]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/youtube-dl/youtube-dl-2021.12.17'
   dh_clean
 dpkg-source -b .
dpkg-source: warning: native package version may not have a revision
dpkg-source: info: using source format '3.0 (native)'
dpkg-source: info: building youtube-dl in youtube-dl_2021.12.17-2+11.0trisquel3.tar.xz
dpkg-source: info: building youtube-dl in youtube-dl_2021.12.17-2+11.0trisquel3.dsc
 debian/rules binary
dh binary
   dh_update_autotools_config
   dh_autoreconf
   dh_auto_configure
   debian/rules override_dh_auto_build
make[1]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/youtube-dl/youtube-dl-2021.12.17'
hi mom!
make[1]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/youtube-dl/youtube-dl-2021.12.17'
   create-stamp debian/debhelper-build-stamp
   dh_prep
   debian/rules override_dh_auto_install
make[1]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/youtube-dl/youtube-dl-2021.12.17'
hi dad!
make[1]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/youtube-dl/youtube-dl-2021.12.17'
   dh_installdocs
   dh_installchangelogs
   dh_perl
   dh_link
   dh_strip_nondeterminism
   dh_compress
   dh_fixperms
   dh_missing
   dh_installdeb
   dh_gencontrol
   dh_md5sums
   dh_builddeb
pkgstripfiles: processing control file: debian/youtube-dl/DEBIAN/control, package youtube-dl, directory debian/youtube-dl
pkgstripfiles: Truncating usr/share/doc/youtube-dl/changelog.Debian.gz to topmost ten records
pkgstripfiles: Running PNG optimization (using 1 cpus) for package youtube-dl ...
pkgstripfiles: No PNG files.
dpkg-deb: building package 'youtube-dl' in '../youtube-dl_2021.12.17-2+11.0trisquel3_all.deb'.
 dpkg-genbuildinfo -O../youtube-dl_2021.12.17-2+11.0trisquel3_amd64.buildinfo
 dpkg-genchanges -O../youtube-dl_2021.12.17-2+11.0trisquel3_amd64.changes
dpkg-genchanges: info: including full source code in upload
 dpkg-source --after-build .
dpkg-buildpackage: info: full upload; Debian-native package (full source is included)
+ cd ..
+ ls -la
total 2872
drwxr-xr-x 3 root root    4096 Apr  2 09:34 .
drwxr-xr-x 3 root root    4096 Apr  2 09:33 ..
-rw-r--r-- 1 root root    9780 Apr  2 09:34 buildlog.txt
drwxr-xr-x 9 root root    4096 Jan 14 06:39 youtube-dl-2021.12.17
-rw-r--r-- 1 root root     864 Apr  2 09:33 youtube-dl_2021.12.17-2+11.0trisquel3.dsc
-rw-r--r-- 1 root root 2879624 Apr  2 09:33 youtube-dl_2021.12.17-2+11.0trisquel3.tar.xz
-rw-r--r-- 1 root root   13136 Apr  2 09:34 youtube-dl_2021.12.17-2+11.0trisquel3_all.deb
-rw-r--r-- 1 root root    6642 Apr  2 09:34 youtube-dl_2021.12.17-2+11.0trisquel3_amd64.buildinfo
-rw-r--r-- 1 root root    1848 Apr  2 09:34 youtube-dl_2021.12.17-2+11.0trisquel3_amd64.changes
+ find . -maxdepth 1 -type f
+ sha256sum ./youtube-dl_2021.12.17-2+11.0trisquel3_amd64.buildinfo ./youtube-dl_2021.12.17-2+11.0trisquel3.dsc ./youtube-dl_2021.12.17-2+11.0trisquel3_amd64.changes ./buildlog.txt ./youtube-dl_2021.12.17-2+11.0trisquel3.tar.xz ./youtube-dl_2021.12.17-2+11.0trisquel3_all.deb
1d80d275a9ace934d77bcfd479bca182753352e4d1588efaeef4fb63d65510ac  ./youtube-dl_2021.12.17-2+11.0trisquel3_amd64.buildinfo
93b277788a6cc4b4c9dca7791372df8f85aa27e08cbdb06455b8be32ae116a18  ./youtube-dl_2021.12.17-2+11.0trisquel3.dsc
bbbe7f39d6bddc72de4604f7366b86d2092fe97b32eef3a4491687c54e0eaf18  ./youtube-dl_2021.12.17-2+11.0trisquel3_amd64.changes
74ddee821ee1e8e8247461a57427a713be2231cd930cf44a703c435ee21ca971  ./buildlog.txt
51d578e073a73975a4d04a85499bb15072888fcc39fbf75342b8b61f8bc40338  ./youtube-dl_2021.12.17-2+11.0trisquel3.tar.xz
b87bcc813f65ca0a5389cf08859bfe25dc69ebc9a0b9558aab876b288dc3f2f9  ./youtube-dl_2021.12.17-2+11.0trisquel3_all.deb
+ mkdir published
+ cd published
+ sed -e s,.*/\([^_]*\)_.*,\1,
+ echo ../youtube-dl_2021.12.17-2+11.0trisquel3_all.deb
+ deb=youtube-dl
+ cut -d' -f2
+ grep ^'
+ apt-get --print-uris --yes download youtube-dl
+ url=http://archive.trisquel.info/trisquel/pool/main/y/youtube-dl/youtube-dl_2021.12.17-2%2b11.0trisquel3_all.deb
+ wget -q http://archive.trisquel.info/trisquel/pool/main/y/youtube-dl/youtube-dl_2021.12.17-2%2b11.0trisquel3_all.deb
+ tee ../SHA256SUMS
+ find . -maxdepth 1 -type f
+ sha256sum ./youtube-dl_2021.12.17-2+11.0trisquel3_all.deb
b87bcc813f65ca0a5389cf08859bfe25dc69ebc9a0b9558aab876b288dc3f2f9  ./youtube-dl_2021.12.17-2+11.0trisquel3_all.deb
+ cd ..
+ sha256sum -c SHA256SUMS
./youtube-dl_2021.12.17-2+11.0trisquel3_all.deb: OK
+ echo Package youtube-dl is reproducible!
Package youtube-dl is reproducible!
