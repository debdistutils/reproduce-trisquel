+ date
Tue Apr 11 19:12:43 UTC 2023
+ apt-get source --only-source trisquel-mini-data=11.2
Reading package lists...
Need to get 94.4 kB of source archives.
Get:1 http://archive.trisquel.info/trisquel aramo/main trisquel-mini-data 11.2 (dsc) [1473 B]
Get:2 http://archive.trisquel.info/trisquel aramo/main trisquel-mini-data 11.2 (tar) [92.9 kB]
dpkg-source: info: extracting trisquel-mini-data in trisquel-mini-data-11.2
dpkg-source: info: unpacking trisquel-mini-data_11.2.tar.gz
Fetched 94.4 kB in 0s (579 kB/s)
W: Download is performed unsandboxed as root as file 'trisquel-mini-data_11.2.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source trisquel-mini-data=11.2
Reading package lists...
Building dependency tree...
Reading state information...
The following NEW packages will be installed:
  autoconf automake autopoint autotools-dev debhelper debugedit dh-autoreconf
  dh-strip-nondeterminism dwz libdebhelper-perl
  libfile-stripnondeterminism-perl libsub-override-perl libtool m4 po-debconf
0 upgraded, 15 newly installed, 0 to remove and 11 not upgraded.
Need to get 3149 kB of archives.
After this operation, 8859 kB of additional disk space will be used.
Get:1 http://archive.trisquel.info/trisquel aramo/main amd64 m4 amd64 1.4.18-5ubuntu2 [199 kB]
Get:2 http://archive.trisquel.info/trisquel aramo/main amd64 autoconf all 2.71-2 [338 kB]
Get:3 http://archive.trisquel.info/trisquel aramo/main amd64 autotools-dev all 20220109.1 [44.9 kB]
Get:4 http://archive.trisquel.info/trisquel aramo/main amd64 automake all 1:1.16.5-1.3 [558 kB]
Get:5 http://archive.trisquel.info/trisquel aramo/main amd64 autopoint all 0.21-4ubuntu4 [422 kB]
Get:6 http://archive.trisquel.info/trisquel aramo/main amd64 libdebhelper-perl all 13.6ubuntu1 [67.2 kB]
Get:7 http://archive.trisquel.info/trisquel aramo/main amd64 libtool all 2.4.6-15build2 [164 kB]
Get:8 http://archive.trisquel.info/trisquel aramo/main amd64 dh-autoreconf all 20 [16.1 kB]
Get:9 http://archive.trisquel.info/trisquel aramo/main amd64 libsub-override-perl all 0.09-2 [9532 B]
Get:10 http://archive.trisquel.info/trisquel aramo/main amd64 libfile-stripnondeterminism-perl all 1.13.0-1 [18.1 kB]
Get:11 http://archive.trisquel.info/trisquel aramo/main amd64 dh-strip-nondeterminism all 1.13.0-1 [5344 B]
Get:12 http://archive.trisquel.info/trisquel aramo/main amd64 debugedit amd64 1:5.0-4build1 [47.2 kB]
Get:13 http://archive.trisquel.info/trisquel aramo/main amd64 dwz amd64 0.14-1build2 [105 kB]
Get:14 http://archive.trisquel.info/trisquel aramo/main amd64 po-debconf all 1.0.21+nmu1 [233 kB]
Get:15 http://archive.trisquel.info/trisquel aramo/main amd64 debhelper all 13.6ubuntu1 [923 kB]
debconf: delaying package configuration, since apt-utils is not installed
Fetched 3149 kB in 0s (10.9 MB/s)
Selecting previously unselected package m4.
(Reading database ... (Reading database ... 5%(Reading database ... 10%(Reading database ... 15%(Reading database ... 20%(Reading database ... 25%(Reading database ... 30%(Reading database ... 35%(Reading database ... 40%(Reading database ... 45%(Reading database ... 50%(Reading database ... 55%(Reading database ... 60%(Reading database ... 65%(Reading database ... 70%(Reading database ... 75%(Reading database ... 80%(Reading database ... 85%(Reading database ... 90%(Reading database ... 95%(Reading database ... 100%(Reading database ... 123238 files and directories currently installed.)
Preparing to unpack .../00-m4_1.4.18-5ubuntu2_amd64.deb ...
Unpacking m4 (1.4.18-5ubuntu2) ...
Selecting previously unselected package autoconf.
Preparing to unpack .../01-autoconf_2.71-2_all.deb ...
Unpacking autoconf (2.71-2) ...
Selecting previously unselected package autotools-dev.
Preparing to unpack .../02-autotools-dev_20220109.1_all.deb ...
Unpacking autotools-dev (20220109.1) ...
Selecting previously unselected package automake.
Preparing to unpack .../03-automake_1%3a1.16.5-1.3_all.deb ...
Unpacking automake (1:1.16.5-1.3) ...
Selecting previously unselected package autopoint.
Preparing to unpack .../04-autopoint_0.21-4ubuntu4_all.deb ...
Unpacking autopoint (0.21-4ubuntu4) ...
Selecting previously unselected package libdebhelper-perl.
Preparing to unpack .../05-libdebhelper-perl_13.6ubuntu1_all.deb ...
Unpacking libdebhelper-perl (13.6ubuntu1) ...
Selecting previously unselected package libtool.
Preparing to unpack .../06-libtool_2.4.6-15build2_all.deb ...
Unpacking libtool (2.4.6-15build2) ...
Selecting previously unselected package dh-autoreconf.
Preparing to unpack .../07-dh-autoreconf_20_all.deb ...
Unpacking dh-autoreconf (20) ...
Selecting previously unselected package libsub-override-perl.
Preparing to unpack .../08-libsub-override-perl_0.09-2_all.deb ...
Unpacking libsub-override-perl (0.09-2) ...
Selecting previously unselected package libfile-stripnondeterminism-perl.
Preparing to unpack .../09-libfile-stripnondeterminism-perl_1.13.0-1_all.deb ...
Unpacking libfile-stripnondeterminism-perl (1.13.0-1) ...
Selecting previously unselected package dh-strip-nondeterminism.
Preparing to unpack .../10-dh-strip-nondeterminism_1.13.0-1_all.deb ...
Unpacking dh-strip-nondeterminism (1.13.0-1) ...
Selecting previously unselected package debugedit.
Preparing to unpack .../11-debugedit_1%3a5.0-4build1_amd64.deb ...
Unpacking debugedit (1:5.0-4build1) ...
Selecting previously unselected package dwz.
Preparing to unpack .../12-dwz_0.14-1build2_amd64.deb ...
Unpacking dwz (0.14-1build2) ...
Selecting previously unselected package po-debconf.
Preparing to unpack .../13-po-debconf_1.0.21+nmu1_all.deb ...
Unpacking po-debconf (1.0.21+nmu1) ...
Selecting previously unselected package debhelper.
Preparing to unpack .../14-debhelper_13.6ubuntu1_all.deb ...
Unpacking debhelper (13.6ubuntu1) ...
Setting up po-debconf (1.0.21+nmu1) ...
Setting up libdebhelper-perl (13.6ubuntu1) ...
Setting up m4 (1.4.18-5ubuntu2) ...
Setting up autotools-dev (20220109.1) ...
Setting up autopoint (0.21-4ubuntu4) ...
Setting up autoconf (2.71-2) ...
Setting up dwz (0.14-1build2) ...
Setting up debugedit (1:5.0-4build1) ...
Setting up libsub-override-perl (0.09-2) ...
Setting up automake (1:1.16.5-1.3) ...
update-alternatives: using /usr/bin/automake-1.16 to provide /usr/bin/automake (automake) in auto mode
Setting up libfile-stripnondeterminism-perl (1.13.0-1) ...
Setting up libtool (2.4.6-15build2) ...
Setting up dh-autoreconf (20) ...
Setting up dh-strip-nondeterminism (1.13.0-1) ...
Setting up debhelper (13.6ubuntu1) ...
Processing triggers for man-db (2.10.2-1) ...
+ find . -maxdepth 1 -name trisquel-mini-data* -type d
+ cd ./trisquel-mini-data-11.2
+ eatmydata env DEB_BUILD_OPTIONS=noautodbgsym dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package trisquel-mini-data
dpkg-buildpackage: info: source version 11.2
dpkg-buildpackage: info: source distribution aramo
dpkg-buildpackage: info: source changed by Trisquel GNU/Linux developers <trisquel-devel@listas.trisquel.info>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 debian/rules clean
dh_testdir
dh_testroot
rm -f build-stamp
dh_clean
 dpkg-source -b .
dpkg-source: warning: no source format specified in debian/source/format, see dpkg-source(1)
dpkg-source: info: using source format '1.0'
dpkg-source: info: building trisquel-mini-data in trisquel-mini-data_11.2.tar.gz
dpkg-source: info: building trisquel-mini-data in trisquel-mini-data_11.2.dsc
 debian/rules build
dh_testdir
touch build-stamp
 debian/rules binary
dh_testdir
dh_testroot
dh_prep
dh_installdirs
/usr/bin/make DESTDIR=/build/trisquel-mini-data/trisquel-mini-data-11.2/debian/trisquel-mini-data
make[1]: Entering directory '/build/trisquel-mini-data/trisquel-mini-data-11.2'
mkdir -pv /build/trisquel-mini-data/trisquel-mini-data-11.2/debian/trisquel-mini-data
cp -a etc usr /build/trisquel-mini-data/trisquel-mini-data-11.2/debian/trisquel-mini-data/.
# po generation
for i in ; do \
	make -C /build/trisquel-mini-data/trisquel-mini-data-11.2/debian/trisquel-mini-data/$i; \
	rm -rf /build/trisquel-mini-data/trisquel-mini-data-11.2/debian/trisquel-mini-data/$i; \
done
# remove some remaining files
find /build/trisquel-mini-data/trisquel-mini-data-11.2/debian/trisquel-mini-data -type f -iname "*.in" | xargs rm -f
make[1]: Leaving directory '/build/trisquel-mini-data/trisquel-mini-data-11.2'
dh_testdir
dh_testroot
dh_installchangelogs
dh_installdocs
dh_compress
dh_fixperms
dh_installdeb
dh_gencontrol
dh_md5sums
dh_builddeb
pkgstripfiles: processing control file: debian/trisquel-mini-data/DEBIAN/control, package trisquel-mini-data, directory debian/trisquel-mini-data
pkgstripfiles: Truncating usr/share/doc/trisquel-mini-data/changelog.gz to topmost ten records
pkgstripfiles: Running PNG optimization (using 1 cpus) for package trisquel-mini-data ...
xargs: warning: options --max-lines and --replace/-I/-i are mutually exclusive, ignoring previous --max-lines value
oooooo
pkgstripfiles: PNG optimization (6/0) for package trisquel-mini-data took 6 s
dpkg-deb: building package 'trisquel-mini-data' in '../trisquel-mini-data_11.2_all.deb'.
 dpkg-genbuildinfo -O../trisquel-mini-data_11.2_amd64.buildinfo
 dpkg-genchanges -O../trisquel-mini-data_11.2_amd64.changes
dpkg-genchanges: info: including full source code in upload
 dpkg-source --after-build .
dpkg-buildpackage: info: full upload; Debian-native package (full source is included)
+ date
Tue Apr 11 19:13:01 UTC 2023
+ cd ..
+ ls -la
total 208
drwxr-xr-x 3 root root  4096 Apr 11 19:13 .
drwxr-xr-x 3 root root  4096 Apr 11 19:12 ..
-rw-r--r-- 1 root root  9611 Apr 11 19:13 buildlog.txt
drwxr-xr-x 5 root root  4096 Apr 11 19:12 trisquel-mini-data-11.2
-rw-r--r-- 1 root root   590 Apr 11 19:12 trisquel-mini-data_11.2.dsc
-rw-r--r-- 1 root root 92922 Apr 11 19:12 trisquel-mini-data_11.2.tar.gz
-rw-r--r-- 1 root root 76918 Apr 11 19:13 trisquel-mini-data_11.2_all.deb
-rw-r--r-- 1 root root  6624 Apr 11 19:13 trisquel-mini-data_11.2_amd64.buildinfo
-rw-r--r-- 1 root root  1620 Apr 11 19:13 trisquel-mini-data_11.2_amd64.changes
+ find . -maxdepth 1 -type f
+ sha256sum ./trisquel-mini-data_11.2_amd64.buildinfo ./trisquel-mini-data_11.2_amd64.changes ./trisquel-mini-data_11.2.dsc ./buildlog.txt ./trisquel-mini-data_11.2.tar.gz ./trisquel-mini-data_11.2_all.deb
30703c1de5e69da9858867d009125c09b38a0fb8ca1098b40ea13ab0021d0f91  ./trisquel-mini-data_11.2_amd64.buildinfo
836865099b3f636442d3da39c735a1d591cb0234e217c952620bd78d18eed6b4  ./trisquel-mini-data_11.2_amd64.changes
a4826cfcad4df30f7260bcef9d262733efc8719acd81e33cd588f2a497fa964b  ./trisquel-mini-data_11.2.dsc
d0b1d1a2283265742ba2240ca808117c2f1f7059fa64224f77affcf3de63abbf  ./buildlog.txt
fe798637a3c20c44644225639bffa4cdc6d63b95371f17df5ba5766f72a23bca  ./trisquel-mini-data_11.2.tar.gz
cae7377aee79aada27083ab776a0c91fd5991dbf1b7f87b30f4291b922509d18  ./trisquel-mini-data_11.2_all.deb
+ mkdir published
+ cd published
+ cd ../
+ ls trisquel-mini-data_11.2_all.deb *.udeb
ls: cannot access '*.udeb': No such file or directory
+ wget -q http://archive.trisquel.info/trisquel/pool/main/t/trisquel-mini-data/trisquel-mini-data_11.2_all.deb
+ tee ../SHA256SUMS
+ find . -maxdepth 1 -type f
+ sha256sum ./trisquel-mini-data_11.2_all.deb
cae7377aee79aada27083ab776a0c91fd5991dbf1b7f87b30f4291b922509d18  ./trisquel-mini-data_11.2_all.deb
+ cd ..
+ sha256sum -c SHA256SUMS
./trisquel-mini-data_11.2_all.deb: OK
+ echo Package trisquel-mini-data version 11.2 is reproducible!
Package trisquel-mini-data version 11.2 is reproducible!
