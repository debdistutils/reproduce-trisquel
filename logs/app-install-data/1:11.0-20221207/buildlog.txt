+ date
Tue Apr 11 19:10:33 UTC 2023
+ apt-get source --only-source app-install-data=1:11.0-20221207
Reading package lists...
NOTICE: 'app-install-data' packaging is maintained in the 'Git' version control system at:
https://devel.trisquel.info/chaosmonk/app-install-data.git
Please use:
git clone https://devel.trisquel.info/chaosmonk/app-install-data.git
to retrieve the latest (possibly unreleased) updates to the package.
Need to get 10.4 MB of source archives.
Get:1 http://archive.trisquel.info/trisquel aramo/main app-install-data 1:11.0-20221207 (dsc) [1560 B]
Get:2 http://archive.trisquel.info/trisquel aramo/main app-install-data 1:11.0-20221207 (tar) [10.4 MB]
dpkg-source: info: extracting app-install-data in app-install-data-11.0
dpkg-source: info: unpacking app-install-data_11.0-20221207.tar.gz
Fetched 10.4 MB in 1s (8917 kB/s)
W: Download is performed unsandboxed as root as file 'app-install-data_11.0-20221207.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source app-install-data=1:11.0-20221207
Reading package lists...
Building dependency tree...
Reading state information...
The following NEW packages will be installed:
  autoconf automake autopoint autotools-dev debhelper debugedit dh-autoreconf
  dh-strip-nondeterminism dwz intltool libdebhelper-perl
  libfile-stripnondeterminism-perl libsub-override-perl libtool m4 po-debconf
  rename
0 upgraded, 17 newly installed, 0 to remove and 11 not upgraded.
Need to get 3214 kB of archives.
After this operation, 9093 kB of additional disk space will be used.
Get:1 http://archive.trisquel.info/trisquel aramo/main amd64 m4 amd64 1.4.18-5ubuntu2 [199 kB]
Get:2 http://archive.trisquel.info/trisquel aramo/main amd64 autoconf all 2.71-2 [338 kB]
Get:3 http://archive.trisquel.info/trisquel aramo/main amd64 autotools-dev all 20220109.1 [44.9 kB]
Get:4 http://archive.trisquel.info/trisquel aramo/main amd64 automake all 1:1.16.5-1.3 [558 kB]
Get:5 http://archive.trisquel.info/trisquel aramo/main amd64 autopoint all 0.21-4ubuntu4 [422 kB]
Get:6 http://archive.trisquel.info/trisquel aramo/main amd64 libdebhelper-perl all 13.6ubuntu1 [67.2 kB]
Get:7 http://archive.trisquel.info/trisquel aramo/main amd64 libtool all 2.4.6-15build2 [164 kB]
Get:8 http://archive.trisquel.info/trisquel aramo/main amd64 dh-autoreconf all 20 [16.1 kB]
Get:9 http://archive.trisquel.info/trisquel aramo/main amd64 libsub-override-perl all 0.09-2 [9532 B]
Get:10 http://archive.trisquel.info/trisquel aramo/main amd64 libfile-stripnondeterminism-perl all 1.13.0-1 [18.1 kB]
Get:11 http://archive.trisquel.info/trisquel aramo/main amd64 dh-strip-nondeterminism all 1.13.0-1 [5344 B]
Get:12 http://archive.trisquel.info/trisquel aramo/main amd64 debugedit amd64 1:5.0-4build1 [47.2 kB]
Get:13 http://archive.trisquel.info/trisquel aramo/main amd64 dwz amd64 0.14-1build2 [105 kB]
Get:14 http://archive.trisquel.info/trisquel aramo/main amd64 po-debconf all 1.0.21+nmu1 [233 kB]
Get:15 http://archive.trisquel.info/trisquel aramo/main amd64 debhelper all 13.6ubuntu1 [923 kB]
Get:16 http://archive.trisquel.info/trisquel aramo/main amd64 intltool all 0.51.0-6 [44.6 kB]
Get:17 http://archive.trisquel.info/trisquel aramo/main amd64 rename all 1.30-1 [20.2 kB]
debconf: delaying package configuration, since apt-utils is not installed
Fetched 3214 kB in 1s (6251 kB/s)
Selecting previously unselected package m4.
(Reading database ... (Reading database ... 5%(Reading database ... 10%(Reading database ... 15%(Reading database ... 20%(Reading database ... 25%(Reading database ... 30%(Reading database ... 35%(Reading database ... 40%(Reading database ... 45%(Reading database ... 50%(Reading database ... 55%(Reading database ... 60%(Reading database ... 65%(Reading database ... 70%(Reading database ... 75%(Reading database ... 80%(Reading database ... 85%(Reading database ... 90%(Reading database ... 95%(Reading database ... 100%(Reading database ... 123238 files and directories currently installed.)
Preparing to unpack .../00-m4_1.4.18-5ubuntu2_amd64.deb ...
Unpacking m4 (1.4.18-5ubuntu2) ...
Selecting previously unselected package autoconf.
Preparing to unpack .../01-autoconf_2.71-2_all.deb ...
Unpacking autoconf (2.71-2) ...
Selecting previously unselected package autotools-dev.
Preparing to unpack .../02-autotools-dev_20220109.1_all.deb ...
Unpacking autotools-dev (20220109.1) ...
Selecting previously unselected package automake.
Preparing to unpack .../03-automake_1%3a1.16.5-1.3_all.deb ...
Unpacking automake (1:1.16.5-1.3) ...
Selecting previously unselected package autopoint.
Preparing to unpack .../04-autopoint_0.21-4ubuntu4_all.deb ...
Unpacking autopoint (0.21-4ubuntu4) ...
Selecting previously unselected package libdebhelper-perl.
Preparing to unpack .../05-libdebhelper-perl_13.6ubuntu1_all.deb ...
Unpacking libdebhelper-perl (13.6ubuntu1) ...
Selecting previously unselected package libtool.
Preparing to unpack .../06-libtool_2.4.6-15build2_all.deb ...
Unpacking libtool (2.4.6-15build2) ...
Selecting previously unselected package dh-autoreconf.
Preparing to unpack .../07-dh-autoreconf_20_all.deb ...
Unpacking dh-autoreconf (20) ...
Selecting previously unselected package libsub-override-perl.
Preparing to unpack .../08-libsub-override-perl_0.09-2_all.deb ...
Unpacking libsub-override-perl (0.09-2) ...
Selecting previously unselected package libfile-stripnondeterminism-perl.
Preparing to unpack .../09-libfile-stripnondeterminism-perl_1.13.0-1_all.deb ...
Unpacking libfile-stripnondeterminism-perl (1.13.0-1) ...
Selecting previously unselected package dh-strip-nondeterminism.
Preparing to unpack .../10-dh-strip-nondeterminism_1.13.0-1_all.deb ...
Unpacking dh-strip-nondeterminism (1.13.0-1) ...
Selecting previously unselected package debugedit.
Preparing to unpack .../11-debugedit_1%3a5.0-4build1_amd64.deb ...
Unpacking debugedit (1:5.0-4build1) ...
Selecting previously unselected package dwz.
Preparing to unpack .../12-dwz_0.14-1build2_amd64.deb ...
Unpacking dwz (0.14-1build2) ...
Selecting previously unselected package po-debconf.
Preparing to unpack .../13-po-debconf_1.0.21+nmu1_all.deb ...
Unpacking po-debconf (1.0.21+nmu1) ...
Selecting previously unselected package debhelper.
Preparing to unpack .../14-debhelper_13.6ubuntu1_all.deb ...
Unpacking debhelper (13.6ubuntu1) ...
Selecting previously unselected package intltool.
Preparing to unpack .../15-intltool_0.51.0-6_all.deb ...
Unpacking intltool (0.51.0-6) ...
Selecting previously unselected package rename.
Preparing to unpack .../16-rename_1.30-1_all.deb ...
Unpacking rename (1.30-1) ...
Setting up po-debconf (1.0.21+nmu1) ...
Setting up libdebhelper-perl (13.6ubuntu1) ...
Setting up m4 (1.4.18-5ubuntu2) ...
Setting up rename (1.30-1) ...
update-alternatives: using /usr/bin/file-rename to provide /usr/bin/rename (rename) in auto mode
Setting up autotools-dev (20220109.1) ...
Setting up autopoint (0.21-4ubuntu4) ...
Setting up autoconf (2.71-2) ...
Setting up dwz (0.14-1build2) ...
Setting up debugedit (1:5.0-4build1) ...
Setting up libsub-override-perl (0.09-2) ...
Setting up automake (1:1.16.5-1.3) ...
update-alternatives: using /usr/bin/automake-1.16 to provide /usr/bin/automake (automake) in auto mode
Setting up libfile-stripnondeterminism-perl (1.13.0-1) ...
Setting up libtool (2.4.6-15build2) ...
Setting up dh-autoreconf (20) ...
Setting up intltool (0.51.0-6) ...
Setting up dh-strip-nondeterminism (1.13.0-1) ...
Setting up debhelper (13.6ubuntu1) ...
Processing triggers for man-db (2.10.2-1) ...
+ find . -maxdepth 1 -name app-install-data* -type d
+ cd ./app-install-data-11.0
+ eatmydata env DEB_BUILD_OPTIONS=noautodbgsym dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package app-install-data
dpkg-buildpackage: info: source version 1:11.0-20221207
dpkg-buildpackage: info: source distribution aramo
dpkg-buildpackage: info: source changed by Trisquel GNU/Linux developers <trisquel-devel@listas.trisquel.info>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 debian/rules clean
dh clean
dh: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   debian/rules override_dh_auto_clean
make[1]: Entering directory '/build/app-install-data/app-install-data-11.0'
rm -rf util/apt/lists
rm -rf util/lists
rm -rf util/menu-data
rm -rf util/lists
make[1]: Leaving directory '/build/app-install-data/app-install-data-11.0'
   dh_clean
dh_clean: warning: Compatibility levels before 10 are deprecated (level 9 in use)
 dpkg-source -b .
dpkg-source: warning: no source format specified in debian/source/format, see dpkg-source(1)
dpkg-source: warning: native package version may not have a revision
dpkg-source: info: using source format '1.0'
dpkg-source: info: building app-install-data in app-install-data_11.0-20221207.tar.gz
dpkg-source: info: building app-install-data in app-install-data_11.0-20221207.dsc
 debian/rules build
dh build
dh: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_update_autotools_config
   debian/rules override_dh_auto_build
make[1]: Entering directory '/build/app-install-data/app-install-data-11.0'
echo "Now running desktopize"
Now running desktopize
if [ ! -e po/POTFILES.in ]; then \
	./desktopize.sh; \
fi
rename .desktop files in menu-data...
marking translatable values as such in menu-data...
populating potfiles.in and creating pot file...
rename .desktop.in files in menu-data...
unmark translatable values in menu-data...
should be any gettext-domain defined in the desktop files, we dont need it
instead, add our own domain to the files, this will be interesting later
make[1]: Leaving directory '/build/app-install-data/app-install-data-11.0'
 debian/rules binary
dh binary
dh: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_testroot
   dh_prep
   dh_installdirs
dh_installdirs: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_auto_install --destdir=debian/app-install-data/
dh_auto_install: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_install
dh_install: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_installdocs
dh_installdocs: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_installchangelogs
   dh_perl
   dh_link
   dh_strip_nondeterminism
   dh_compress
dh_compress: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_fixperms
   dh_missing
dh_missing: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_installdeb
dh_installdeb: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_gencontrol
dpkg-gencontrol: warning: package app-install-data: substitution variable ${python:Versions} used, but is not defined
   dh_md5sums
   debian/rules override_dh_builddeb
make[1]: Entering directory '/build/app-install-data/app-install-data-11.0'
dh_builddeb -- -Zxz
pkgstripfiles: processing control file: debian/app-install-data/DEBIAN/control, package app-install-data, directory debian/app-install-data
pkgstripfiles: Running PNG optimization (using 1 cpus) for package app-install-data ...
xargs: warning: options --max-lines and --replace/-I/-i are mutually exclusive, ignoring previous --max-lines value
o
pkgstripfiles: PNG optimization (1/0) for package app-install-data took 1 s
dpkg-deb: building package 'app-install-data' in '../app-install-data_11.0-20221207_all.deb'.
make[1]: Leaving directory '/build/app-install-data/app-install-data-11.0'
 dpkg-genbuildinfo -O../app-install-data_11.0-20221207_amd64.buildinfo
 dpkg-genchanges -O../app-install-data_11.0-20221207_amd64.changes
dpkg-genchanges: info: including full source code in upload
 dpkg-source --after-build .
dpkg-buildpackage: info: full upload; Debian-native package (full source is included)
+ date
Tue Apr 11 19:13:01 UTC 2023
+ cd ..
+ ls -la
total 18956
drwxr-xr-x 3 root root     4096 Apr 11 19:13 .
drwxr-xr-x 3 root root     4096 Apr 11 19:10 ..
drwxr-xr-x 8 root root     4096 Dec  7 08:57 app-install-data-11.0
-rw-r--r-- 1 root root      677 Apr 11 19:10 app-install-data_11.0-20221207.dsc
-rw-r--r-- 1 root root 10431741 Apr 11 19:10 app-install-data_11.0-20221207.tar.gz
-rw-r--r-- 1 root root  8937376 Apr 11 19:13 app-install-data_11.0-20221207_all.deb
-rw-r--r-- 1 root root     7606 Apr 11 19:13 app-install-data_11.0-20221207_amd64.buildinfo
-rw-r--r-- 1 root root     1714 Apr 11 19:13 app-install-data_11.0-20221207_amd64.changes
-rw-r--r-- 1 root root    12085 Apr 11 19:13 buildlog.txt
+ find . -maxdepth 1 -type f
+ sha256sum ./app-install-data_11.0-20221207_all.deb ./app-install-data_11.0-20221207_amd64.buildinfo ./buildlog.txt ./app-install-data_11.0-20221207.dsc ./app-install-data_11.0-20221207.tar.gz ./app-install-data_11.0-20221207_amd64.changes
ea7aca5203855cfbc96cc0cccfb8ecada0a5c99c9e41f41f562f2b61d084d42d  ./app-install-data_11.0-20221207_all.deb
62d10299f44217859d86a637eafe6b620e1f25ed93da5a05b5952b3e186a7161  ./app-install-data_11.0-20221207_amd64.buildinfo
1ad54a82bd75283f6433ce26ffe3feb181a76439b6f4943b9028a966107d10c5  ./buildlog.txt
0fdb51cc716ec561397714b6542f4f4aaec8640322a47ddc3c6afd574772c874  ./app-install-data_11.0-20221207.dsc
2bc48e8611f16203fc868a8238e8dfa391aabafd32978d2b6087c7e33f0e0b13  ./app-install-data_11.0-20221207.tar.gz
24be0c0034a25f177cddb2a8f135bcbd00580f593bd4b92b4b7042d48fe52b35  ./app-install-data_11.0-20221207_amd64.changes
+ mkdir published
+ cd published
+ cd ../
+ ls app-install-data_11.0-20221207_all.deb *.udeb
ls: cannot access '*.udeb': No such file or directory
+ wget -q http://archive.trisquel.info/trisquel/pool/main/a/app-install-data/app-install-data_11.0-20221207_all.deb
+ tee ../SHA256SUMS
+ find . -maxdepth 1 -type f
+ sha256sum ./app-install-data_11.0-20221207_all.deb
ea7aca5203855cfbc96cc0cccfb8ecada0a5c99c9e41f41f562f2b61d084d42d  ./app-install-data_11.0-20221207_all.deb
+ cd ..
+ sha256sum -c SHA256SUMS
./app-install-data_11.0-20221207_all.deb: OK
+ echo Package app-install-data version 1:11.0-20221207 is reproducible!
Package app-install-data version 1:11.0-20221207 is reproducible!
