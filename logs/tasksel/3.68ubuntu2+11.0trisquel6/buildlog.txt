+ date
Sun Apr  2 09:15:29 UTC 2023
+ apt-get source --only-source tasksel
Reading package lists...
NOTICE: 'tasksel' packaging is maintained in the 'Git' version control system at:
https://salsa.debian.org/installer-team/tasksel.git
Please use:
git clone https://salsa.debian.org/installer-team/tasksel.git
to retrieve the latest (possibly unreleased) updates to the package.
Need to get 292 kB of source archives.
Get:1 http://archive.trisquel.info/trisquel aramo/main tasksel 3.68ubuntu2+11.0trisquel6 (dsc) [1846 B]
Get:2 http://archive.trisquel.info/trisquel aramo/main tasksel 3.68ubuntu2+11.0trisquel6 (tar) [290 kB]
dpkg-source: info: extracting tasksel in tasksel-3.68ubuntu2+11.0trisquel6
dpkg-source: info: unpacking tasksel_3.68ubuntu2+11.0trisquel6.tar.xz
Fetched 292 kB in 1s (399 kB/s)
W: Download is performed unsandboxed as root as file 'tasksel_3.68ubuntu2+11.0trisquel6.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source tasksel
Reading package lists...
Building dependency tree...
Reading state information...
The following NEW packages will be installed:
  autoconf automake autopoint autotools-dev debhelper debugedit dh-autoreconf
  dh-strip-nondeterminism dwz libdebhelper-perl
  libfile-stripnondeterminism-perl libsub-override-perl libtool m4 po-debconf
0 upgraded, 15 newly installed, 0 to remove and 1 not upgraded.
Need to get 3149 kB of archives.
After this operation, 8859 kB of additional disk space will be used.
Get:1 http://archive.trisquel.info/trisquel aramo/main amd64 m4 amd64 1.4.18-5ubuntu2 [199 kB]
Get:2 http://archive.trisquel.info/trisquel aramo/main amd64 autoconf all 2.71-2 [338 kB]
Get:3 http://archive.trisquel.info/trisquel aramo/main amd64 autotools-dev all 20220109.1 [44.9 kB]
Get:4 http://archive.trisquel.info/trisquel aramo/main amd64 automake all 1:1.16.5-1.3 [558 kB]
Get:5 http://archive.trisquel.info/trisquel aramo/main amd64 autopoint all 0.21-4ubuntu4 [422 kB]
Get:6 http://archive.trisquel.info/trisquel aramo/main amd64 libdebhelper-perl all 13.6ubuntu1 [67.2 kB]
Get:7 http://archive.trisquel.info/trisquel aramo/main amd64 libtool all 2.4.6-15build2 [164 kB]
Get:8 http://archive.trisquel.info/trisquel aramo/main amd64 dh-autoreconf all 20 [16.1 kB]
Get:9 http://archive.trisquel.info/trisquel aramo/main amd64 libsub-override-perl all 0.09-2 [9532 B]
Get:10 http://archive.trisquel.info/trisquel aramo/main amd64 libfile-stripnondeterminism-perl all 1.13.0-1 [18.1 kB]
Get:11 http://archive.trisquel.info/trisquel aramo/main amd64 dh-strip-nondeterminism all 1.13.0-1 [5344 B]
Get:12 http://archive.trisquel.info/trisquel aramo/main amd64 debugedit amd64 1:5.0-4build1 [47.2 kB]
Get:13 http://archive.trisquel.info/trisquel aramo/main amd64 dwz amd64 0.14-1build2 [105 kB]
Get:14 http://archive.trisquel.info/trisquel aramo/main amd64 po-debconf all 1.0.21+nmu1 [233 kB]
Get:15 http://archive.trisquel.info/trisquel aramo/main amd64 debhelper all 13.6ubuntu1 [923 kB]
debconf: delaying package configuration, since apt-utils is not installed
Fetched 3149 kB in 0s (10.8 MB/s)
Selecting previously unselected package m4.
(Reading database ... (Reading database ... 5%(Reading database ... 10%(Reading database ... 15%(Reading database ... 20%(Reading database ... 25%(Reading database ... 30%(Reading database ... 35%(Reading database ... 40%(Reading database ... 45%(Reading database ... 50%(Reading database ... 55%(Reading database ... 60%(Reading database ... 65%(Reading database ... 70%(Reading database ... 75%(Reading database ... 80%(Reading database ... 85%(Reading database ... 90%(Reading database ... 95%(Reading database ... 100%(Reading database ... 123231 files and directories currently installed.)
Preparing to unpack .../00-m4_1.4.18-5ubuntu2_amd64.deb ...
Unpacking m4 (1.4.18-5ubuntu2) ...
Selecting previously unselected package autoconf.
Preparing to unpack .../01-autoconf_2.71-2_all.deb ...
Unpacking autoconf (2.71-2) ...
Selecting previously unselected package autotools-dev.
Preparing to unpack .../02-autotools-dev_20220109.1_all.deb ...
Unpacking autotools-dev (20220109.1) ...
Selecting previously unselected package automake.
Preparing to unpack .../03-automake_1%3a1.16.5-1.3_all.deb ...
Unpacking automake (1:1.16.5-1.3) ...
Selecting previously unselected package autopoint.
Preparing to unpack .../04-autopoint_0.21-4ubuntu4_all.deb ...
Unpacking autopoint (0.21-4ubuntu4) ...
Selecting previously unselected package libdebhelper-perl.
Preparing to unpack .../05-libdebhelper-perl_13.6ubuntu1_all.deb ...
Unpacking libdebhelper-perl (13.6ubuntu1) ...
Selecting previously unselected package libtool.
Preparing to unpack .../06-libtool_2.4.6-15build2_all.deb ...
Unpacking libtool (2.4.6-15build2) ...
Selecting previously unselected package dh-autoreconf.
Preparing to unpack .../07-dh-autoreconf_20_all.deb ...
Unpacking dh-autoreconf (20) ...
Selecting previously unselected package libsub-override-perl.
Preparing to unpack .../08-libsub-override-perl_0.09-2_all.deb ...
Unpacking libsub-override-perl (0.09-2) ...
Selecting previously unselected package libfile-stripnondeterminism-perl.
Preparing to unpack .../09-libfile-stripnondeterminism-perl_1.13.0-1_all.deb ...
Unpacking libfile-stripnondeterminism-perl (1.13.0-1) ...
Selecting previously unselected package dh-strip-nondeterminism.
Preparing to unpack .../10-dh-strip-nondeterminism_1.13.0-1_all.deb ...
Unpacking dh-strip-nondeterminism (1.13.0-1) ...
Selecting previously unselected package debugedit.
Preparing to unpack .../11-debugedit_1%3a5.0-4build1_amd64.deb ...
Unpacking debugedit (1:5.0-4build1) ...
Selecting previously unselected package dwz.
Preparing to unpack .../12-dwz_0.14-1build2_amd64.deb ...
Unpacking dwz (0.14-1build2) ...
Selecting previously unselected package po-debconf.
Preparing to unpack .../13-po-debconf_1.0.21+nmu1_all.deb ...
Unpacking po-debconf (1.0.21+nmu1) ...
Selecting previously unselected package debhelper.
Preparing to unpack .../14-debhelper_13.6ubuntu1_all.deb ...
Unpacking debhelper (13.6ubuntu1) ...
Setting up po-debconf (1.0.21+nmu1) ...
Setting up libdebhelper-perl (13.6ubuntu1) ...
Setting up m4 (1.4.18-5ubuntu2) ...
Setting up autotools-dev (20220109.1) ...
Setting up autopoint (0.21-4ubuntu4) ...
Setting up autoconf (2.71-2) ...
Setting up dwz (0.14-1build2) ...
Setting up debugedit (1:5.0-4build1) ...
Setting up libsub-override-perl (0.09-2) ...
Setting up automake (1:1.16.5-1.3) ...
update-alternatives: using /usr/bin/automake-1.16 to provide /usr/bin/automake (automake) in auto mode
Setting up libfile-stripnondeterminism-perl (1.13.0-1) ...
Setting up libtool (2.4.6-15build2) ...
Setting up dh-autoreconf (20) ...
Setting up dh-strip-nondeterminism (1.13.0-1) ...
Setting up debhelper (13.6ubuntu1) ...
Processing triggers for man-db (2.10.2-1) ...
+ find . -maxdepth 1 -name tasksel* -type d
+ cd ./tasksel-3.68ubuntu2+11.0trisquel6
+ eatmydata env DEB_BUILD_OPTIONS=noautodbgsym dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package tasksel
dpkg-buildpackage: info: source version 3.68ubuntu2+11.0trisquel6
dpkg-buildpackage: info: source distribution aramo
dpkg-buildpackage: info: source changed by Trisquel GNU/Linux developers <trisquel-devel@listas.trisquel.info>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 debian/rules clean
dh clean --no-parallel
   dh_auto_clean
	make -j1 clean
make[1]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/tasksel/tasksel-3.68ubuntu2+11.0trisquel6'
rm -f debian-tasks.desc trisquel-tasks.desc *~
rm -rf debian/external-overrides
make -C po clean
make[2]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/tasksel/tasksel-3.68ubuntu2+11.0trisquel6/po'
rm -f build_stamp
rm -f *.mo
make[2]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/tasksel/tasksel-3.68ubuntu2+11.0trisquel6/po'
make -C tasks/po clean
make[2]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/tasksel/tasksel-3.68ubuntu2+11.0trisquel6/tasks/po'
rm -f build_stamp
rm -f debian-tasks.desc
rm -f *.mo
make[2]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/tasksel/tasksel-3.68ubuntu2+11.0trisquel6/tasks/po'
make[1]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/tasksel/tasksel-3.68ubuntu2+11.0trisquel6'
   dh_clean
 dpkg-source -b .
dpkg-source: info: using source format '3.0 (native)'
dpkg-source: info: building tasksel in tasksel_3.68ubuntu2+11.0trisquel6.tar.xz
dpkg-source: info: building tasksel in tasksel_3.68ubuntu2+11.0trisquel6.dsc
 debian/rules binary
dh binary --no-parallel
   dh_update_autotools_config
   dh_autoreconf
   dh_auto_configure
   dh_auto_build
	make -j1 "INSTALL=install --strip-program=true"
make[1]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/tasksel/tasksel-3.68ubuntu2+11.0trisquel6'
./makedesc.pl trisquel-tasks trisquel-tasks.desc
ltsp-server: ltsp-server-standalone is not a valid package.
MISSING KEY PACKAGE, TASK BROKEN
web-server: libapache2-mod-php5 is not a valid package.
MISSING KEY PACKAGE, TASK BROKEN
make -C po LANGS="ar bg bn bs ca cs cy da de dz el eo es et eu fa fi fr gl gu he hi hr hu hy id it ja km ko lt lv mg mk nb ne nl nn pa pl pt_BR pt ro ru sk sl sq sv ta te th tl tr uk vi wo zh_CN zh_TW"
make[2]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/tasksel/tasksel-3.68ubuntu2+11.0trisquel6/po'
msgfmt -o ar.mo ar.po
msgfmt -o bg.mo bg.po
msgfmt -o bn.mo bn.po
msgfmt -o bs.mo bs.po
msgfmt -o ca.mo ca.po
msgfmt -o cs.mo cs.po
msgfmt -o cy.mo cy.po
msgfmt -o da.mo da.po
msgfmt -o de.mo de.po
msgfmt -o dz.mo dz.po
msgfmt -o el.mo el.po
msgfmt -o eo.mo eo.po
msgfmt -o es.mo es.po
msgfmt -o et.mo et.po
msgfmt -o eu.mo eu.po
msgfmt -o fa.mo fa.po
msgfmt -o fi.mo fi.po
msgfmt -o fr.mo fr.po
msgfmt -o gl.mo gl.po
msgfmt -o gu.mo gu.po
msgfmt -o he.mo he.po
msgfmt -o hi.mo hi.po
msgfmt -o hr.mo hr.po
msgfmt -o hu.mo hu.po
msgfmt -o hy.mo hy.po
msgfmt -o id.mo id.po
msgfmt -o it.mo it.po
msgfmt -o ja.mo ja.po
msgfmt -o km.mo km.po
msgfmt -o ko.mo ko.po
msgfmt -o lt.mo lt.po
msgfmt -o lv.mo lv.po
msgfmt -o mg.mo mg.po
msgfmt -o mk.mo mk.po
msgfmt -o nb.mo nb.po
msgfmt -o ne.mo ne.po
msgfmt -o nl.mo nl.po
msgfmt -o nn.mo nn.po
msgfmt -o pa.mo pa.po
msgfmt -o pl.mo pl.po
msgfmt -o pt_BR.mo pt_BR.po
msgfmt -o pt.mo pt.po
msgfmt -o ro.mo ro.po
msgfmt -o ru.mo ru.po
msgfmt -o sk.mo sk.po
msgfmt -o sl.mo sl.po
msgfmt -o sq.mo sq.po
msgfmt -o sv.mo sv.po
msgfmt -o ta.mo ta.po
msgfmt -o te.mo te.po
msgfmt -o th.mo th.po
msgfmt -o tl.mo tl.po
msgfmt -o tr.mo tr.po
msgfmt -o uk.mo uk.po
msgfmt -o vi.mo vi.po
msgfmt -o wo.mo wo.po
msgfmt -o zh_CN.mo zh_CN.po
msgfmt -o zh_TW.mo zh_TW.po
touch build_stamp
make[2]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/tasksel/tasksel-3.68ubuntu2+11.0trisquel6/po'
make[1]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/tasksel/tasksel-3.68ubuntu2+11.0trisquel6'
   dh_auto_test
   create-stamp debian/debhelper-build-stamp
   dh_prep
   debian/rules override_dh_auto_install
make[1]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/tasksel/tasksel-3.68ubuntu2+11.0trisquel6'
/usr/bin/make install DESTDIR=`pwd`/debian/tasksel
make[2]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/tasksel/tasksel-3.68ubuntu2+11.0trisquel6'
install -d /builds/debdistutils/reproduce-trisquel/buildlogs/tasksel/tasksel-3.68ubuntu2+11.0trisquel6/debian/tasksel/usr/bin \
	/builds/debdistutils/reproduce-trisquel/buildlogs/tasksel/tasksel-3.68ubuntu2+11.0trisquel6/debian/tasksel/usr/lib/tasksel/tests \
	/builds/debdistutils/reproduce-trisquel/buildlogs/tasksel/tasksel-3.68ubuntu2+11.0trisquel6/debian/tasksel/usr/lib/tasksel/packages \
	/builds/debdistutils/reproduce-trisquel/buildlogs/tasksel/tasksel-3.68ubuntu2+11.0trisquel6/debian/tasksel/usr/share/man/man8
install -m 755 tasksel.pl /builds/debdistutils/reproduce-trisquel/buildlogs/tasksel/tasksel-3.68ubuntu2+11.0trisquel6/debian/tasksel/usr/bin/tasksel
install -m 755 tasksel-debconf /builds/debdistutils/reproduce-trisquel/buildlogs/tasksel/tasksel-3.68ubuntu2+11.0trisquel6/debian/tasksel/usr/lib/tasksel/
install -m 644 default_desktop /builds/debdistutils/reproduce-trisquel/buildlogs/tasksel/tasksel-3.68ubuntu2+11.0trisquel6/debian/tasksel/usr/lib/tasksel/
install -m 755 filter-tasks /builds/debdistutils/reproduce-trisquel/buildlogs/tasksel/tasksel-3.68ubuntu2+11.0trisquel6/debian/tasksel/usr/lib/tasksel/
install -m 755 tests/new-install /builds/debdistutils/reproduce-trisquel/buildlogs/tasksel/tasksel-3.68ubuntu2+11.0trisquel6/debian/tasksel/usr/lib/tasksel/tests/
install -m 755 tests/debconf /builds/debdistutils/reproduce-trisquel/buildlogs/tasksel/tasksel-3.68ubuntu2+11.0trisquel6/debian/tasksel/usr/lib/tasksel/tests/
install -m 755 tests/lang /builds/debdistutils/reproduce-trisquel/buildlogs/tasksel/tasksel-3.68ubuntu2+11.0trisquel6/debian/tasksel/usr/lib/tasksel/tests/
install -m 755 packages/list /builds/debdistutils/reproduce-trisquel/buildlogs/tasksel/tasksel-3.68ubuntu2+11.0trisquel6/debian/tasksel/usr/lib/tasksel/packages/
pod2man --section=8 --center "Debian specific manpage" --release 3.68ubuntu2+11.0trisquel6 tasksel.pod | gzip -9c > /builds/debdistutils/reproduce-trisquel/buildlogs/tasksel/tasksel-3.68ubuntu2+11.0trisquel6/debian/tasksel/usr/share/man/man8/tasksel.8.gz
for lang in ar bg bn bs ca cs cy da de dz el eo es et eu fa fi fr gl gu he hi hr hu hy id it ja km ko lt lv mg mk nb ne nl nn pa pl pt_BR pt ro ru sk sl sq sv ta te th tl tr uk vi wo zh_CN zh_TW; do \
	[ ! -d /builds/debdistutils/reproduce-trisquel/buildlogs/tasksel/tasksel-3.68ubuntu2+11.0trisquel6/debian/tasksel/usr/share/locale/$lang/LC_MESSAGES/ ] && mkdir -p /builds/debdistutils/reproduce-trisquel/buildlogs/tasksel/tasksel-3.68ubuntu2+11.0trisquel6/debian/tasksel/usr/share/locale/$lang/LC_MESSAGES/; \
	install -m 644 po/$lang.mo /builds/debdistutils/reproduce-trisquel/buildlogs/tasksel/tasksel-3.68ubuntu2+11.0trisquel6/debian/tasksel/usr/share/locale/$lang/LC_MESSAGES/tasksel.mo; \
done
make[2]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/tasksel/tasksel-3.68ubuntu2+11.0trisquel6'
/usr/bin/make install-data DESTDIR=`pwd`/debian/tasksel-data
make[2]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/tasksel/tasksel-3.68ubuntu2+11.0trisquel6'
install -d /builds/debdistutils/reproduce-trisquel/buildlogs/tasksel/tasksel-3.68ubuntu2+11.0trisquel6/debian/tasksel-data/usr/share/tasksel/descs \
	/builds/debdistutils/reproduce-trisquel/buildlogs/tasksel/tasksel-3.68ubuntu2+11.0trisquel6/debian/tasksel-data/usr/lib/tasksel/info \
	/builds/debdistutils/reproduce-trisquel/buildlogs/tasksel/tasksel-3.68ubuntu2+11.0trisquel6/debian/tasksel-data/usr/lib/tasksel/tests
install -m 0644 trisquel-tasks.desc /builds/debdistutils/reproduce-trisquel/buildlogs/tasksel/tasksel-3.68ubuntu2+11.0trisquel6/debian/tasksel-data/usr/share/tasksel/descs
for test in tests/*; do \
	[ "$test" = "tests/new-install" ] && continue; \
	[ "$test" = "tests/debconf" ] && continue; \
	[ "$test" = "tests/lang" ] && continue; \
	install -m 755 $test /builds/debdistutils/reproduce-trisquel/buildlogs/tasksel/tasksel-3.68ubuntu2+11.0trisquel6/debian/tasksel-data/usr/lib/tasksel/tests/; \
done
for flavour in trisquel-desktop triskel trisquel-mini trisquel-sugar trisquel-gnome trisquel-console; do \
	ln -s desktop.preinst /builds/debdistutils/reproduce-trisquel/buildlogs/tasksel/tasksel-3.68ubuntu2+11.0trisquel6/debian/tasksel-data/usr/lib/tasksel/info/$flavour-desktop.preinst; \
done
for package in packages/*; do \
	[ "$package" = "packages/list" ] && continue; \
	install -m 755 $package /builds/debdistutils/reproduce-trisquel/buildlogs/tasksel/tasksel-3.68ubuntu2+11.0trisquel6/debian/tasksel-data/usr/lib/tasksel/packages/; \
done
make[2]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/tasksel/tasksel-3.68ubuntu2+11.0trisquel6'
make[1]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/tasksel/tasksel-3.68ubuntu2+11.0trisquel6'
   debian/rules override_dh_installdocs
make[1]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/tasksel/tasksel-3.68ubuntu2+11.0trisquel6'
dh_installdocs --link-doc=tasksel
make[1]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/tasksel/tasksel-3.68ubuntu2+11.0trisquel6'
   dh_installchangelogs
   dh_installman
   dh_installdebconf
   dh_installmenu
   dh_lintian
   debian/rules override_dh_perl
make[1]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/tasksel/tasksel-3.68ubuntu2+11.0trisquel6'
dh_perl -d
make[1]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/tasksel/tasksel-3.68ubuntu2+11.0trisquel6'
   dh_link
   dh_strip_nondeterminism
	Normalized debian/tasksel/usr/share/locale/sk/LC_MESSAGES/tasksel.mo
	Normalized debian/tasksel/usr/share/locale/ca/LC_MESSAGES/tasksel.mo
	Normalized debian/tasksel/usr/share/locale/sq/LC_MESSAGES/tasksel.mo
	Normalized debian/tasksel/usr/share/locale/bs/LC_MESSAGES/tasksel.mo
	Normalized debian/tasksel/usr/share/locale/uk/LC_MESSAGES/tasksel.mo
	Normalized debian/tasksel/usr/share/locale/bg/LC_MESSAGES/tasksel.mo
	Normalized debian/tasksel/usr/share/locale/mk/LC_MESSAGES/tasksel.mo
	Normalized debian/tasksel/usr/share/locale/eo/LC_MESSAGES/tasksel.mo
	Normalized debian/tasksel/usr/share/locale/pl/LC_MESSAGES/tasksel.mo
	Normalized debian/tasksel/usr/share/locale/te/LC_MESSAGES/tasksel.mo
	Normalized debian/tasksel/usr/share/locale/vi/LC_MESSAGES/tasksel.mo
	Normalized debian/tasksel/usr/share/locale/zh_TW/LC_MESSAGES/tasksel.mo
	Normalized debian/tasksel/usr/share/locale/nn/LC_MESSAGES/tasksel.mo
	Normalized debian/tasksel/usr/share/locale/id/LC_MESSAGES/tasksel.mo
	Normalized debian/tasksel/usr/share/locale/ta/LC_MESSAGES/tasksel.mo
	Normalized debian/tasksel/usr/share/locale/fr/LC_MESSAGES/tasksel.mo
	Normalized debian/tasksel/usr/share/locale/fi/LC_MESSAGES/tasksel.mo
	Normalized debian/tasksel/usr/share/locale/ko/LC_MESSAGES/tasksel.mo
	Normalized debian/tasksel/usr/share/locale/hi/LC_MESSAGES/tasksel.mo
	Normalized debian/tasksel/usr/share/locale/zh_CN/LC_MESSAGES/tasksel.mo
	Normalized debian/tasksel/usr/share/locale/sv/LC_MESSAGES/tasksel.mo
	Normalized debian/tasksel/usr/share/locale/ar/LC_MESSAGES/tasksel.mo
	Normalized debian/tasksel/usr/share/locale/bn/LC_MESSAGES/tasksel.mo
	Normalized debian/tasksel/usr/share/locale/lv/LC_MESSAGES/tasksel.mo
	Normalized debian/tasksel/usr/share/locale/tr/LC_MESSAGES/tasksel.mo
	Normalized debian/tasksel/usr/share/locale/ru/LC_MESSAGES/tasksel.mo
	Normalized debian/tasksel/usr/share/locale/fa/LC_MESSAGES/tasksel.mo
	Normalized debian/tasksel/usr/share/locale/sl/LC_MESSAGES/tasksel.mo
	Normalized debian/tasksel/usr/share/locale/es/LC_MESSAGES/tasksel.mo
	Normalized debian/tasksel/usr/share/locale/et/LC_MESSAGES/tasksel.mo
	Normalized debian/tasksel/usr/share/locale/lt/LC_MESSAGES/tasksel.mo
	Normalized debian/tasksel/usr/share/locale/cy/LC_MESSAGES/tasksel.mo
	Normalized debian/tasksel/usr/share/locale/el/LC_MESSAGES/tasksel.mo
	Normalized debian/tasksel/usr/share/locale/ja/LC_MESSAGES/tasksel.mo
	Normalized debian/tasksel/usr/share/locale/cs/LC_MESSAGES/tasksel.mo
	Normalized debian/tasksel/usr/share/locale/tl/LC_MESSAGES/tasksel.mo
	Normalized debian/tasksel/usr/share/locale/nl/LC_MESSAGES/tasksel.mo
	Normalized debian/tasksel/usr/share/locale/km/LC_MESSAGES/tasksel.mo
	Normalized debian/tasksel/usr/share/locale/dz/LC_MESSAGES/tasksel.mo
	Normalized debian/tasksel/usr/share/locale/pt_BR/LC_MESSAGES/tasksel.mo
	Normalized debian/tasksel/usr/share/locale/de/LC_MESSAGES/tasksel.mo
	Normalized debian/tasksel/usr/share/locale/ne/LC_MESSAGES/tasksel.mo
	Normalized debian/tasksel/usr/share/locale/da/LC_MESSAGES/tasksel.mo
	Normalized debian/tasksel/usr/share/locale/mg/LC_MESSAGES/tasksel.mo
	Normalized debian/tasksel/usr/share/locale/hr/LC_MESSAGES/tasksel.mo
	Normalized debian/tasksel/usr/share/locale/gu/LC_MESSAGES/tasksel.mo
	Normalized debian/tasksel/usr/share/locale/hy/LC_MESSAGES/tasksel.mo
	Normalized debian/tasksel/usr/share/locale/pa/LC_MESSAGES/tasksel.mo
	Normalized debian/tasksel/usr/share/locale/he/LC_MESSAGES/tasksel.mo
	Normalized debian/tasksel/usr/share/locale/hu/LC_MESSAGES/tasksel.mo
	Normalized debian/tasksel/usr/share/locale/th/LC_MESSAGES/tasksel.mo
	Normalized debian/tasksel/usr/share/locale/wo/LC_MESSAGES/tasksel.mo
	Normalized debian/tasksel/usr/share/locale/pt/LC_MESSAGES/tasksel.mo
	Normalized debian/tasksel/usr/share/locale/nb/LC_MESSAGES/tasksel.mo
	Normalized debian/tasksel/usr/share/locale/ro/LC_MESSAGES/tasksel.mo
	Normalized debian/tasksel/usr/share/locale/eu/LC_MESSAGES/tasksel.mo
	Normalized debian/tasksel/usr/share/locale/it/LC_MESSAGES/tasksel.mo
	Normalized debian/tasksel/usr/share/locale/gl/LC_MESSAGES/tasksel.mo
   dh_compress
   dh_fixperms
   dh_missing
   dh_installdeb
   dh_gencontrol
   dh_md5sums
   dh_builddeb
pkgstripfiles: processing control file: debian/tasksel/DEBIAN/control, package tasksel, directory debian/tasksel
Searching for duplicated docs in dependency tasksel-data...
pkgstripfiles: Truncating usr/share/doc/tasksel/changelog.gz to topmost ten records
pkgstripfiles: Running PNG optimization (using 1 cpus) for package tasksel ...
pkgstripfiles: No PNG files.
dpkg-deb: building package 'tasksel' in '../tasksel_3.68ubuntu2+11.0trisquel6_all.deb'.
pkgstripfiles: processing control file: debian/tasksel-data/DEBIAN/control, package tasksel-data, directory debian/tasksel-data
pkgstripfiles: Running PNG optimization (using 1 cpus) for package tasksel-data ...
pkgstripfiles: No PNG files.
dpkg-deb: building package 'tasksel-data' in '../tasksel-data_3.68ubuntu2+11.0trisquel6_all.deb'.
 dpkg-genbuildinfo -O../tasksel_3.68ubuntu2+11.0trisquel6_amd64.buildinfo
 dpkg-genchanges -O../tasksel_3.68ubuntu2+11.0trisquel6_amd64.changes
dpkg-genchanges: info: including full source code in upload
 dpkg-source --after-build .
dpkg-buildpackage: info: full upload; Debian-native package (full source is included)
+ cd ..
+ ls -la
total 392
drwxr-xr-x 3 root root   4096 Apr  2 09:15 .
drwxr-xr-x 3 root root   4096 Apr  2 09:15 ..
-rw-r--r-- 1 root root  22373 Apr  2 09:15 buildlog.txt
drwxr-xr-x 8 root root   4096 Apr  2 09:15 tasksel-3.68ubuntu2+11.0trisquel6
-rw-r--r-- 1 root root   3724 Apr  2 09:15 tasksel-data_3.68ubuntu2+11.0trisquel6_all.deb
-rw-r--r-- 1 root root    963 Apr  2 09:15 tasksel_3.68ubuntu2+11.0trisquel6.dsc
-rw-r--r-- 1 root root 289872 Apr  2 09:15 tasksel_3.68ubuntu2+11.0trisquel6.tar.xz
-rw-r--r-- 1 root root  52888 Apr  2 09:15 tasksel_3.68ubuntu2+11.0trisquel6_all.deb
-rw-r--r-- 1 root root   6922 Apr  2 09:15 tasksel_3.68ubuntu2+11.0trisquel6_amd64.buildinfo
-rw-r--r-- 1 root root   2248 Apr  2 09:15 tasksel_3.68ubuntu2+11.0trisquel6_amd64.changes
+ find . -maxdepth 1 -type f
+ sha256sum ./tasksel_3.68ubuntu2+11.0trisquel6_amd64.changes ./tasksel_3.68ubuntu2+11.0trisquel6.dsc ./buildlog.txt ./tasksel_3.68ubuntu2+11.0trisquel6_amd64.buildinfo ./tasksel-data_3.68ubuntu2+11.0trisquel6_all.deb ./tasksel_3.68ubuntu2+11.0trisquel6.tar.xz ./tasksel_3.68ubuntu2+11.0trisquel6_all.deb
3d03b4b91333cc7ca310e6d1782f3efd5739f241ae3b64af0869e9c81b003c5c  ./tasksel_3.68ubuntu2+11.0trisquel6_amd64.changes
1614708bdb8d27346c8075bc326616a39f971522d51d9de35aa7c4a756571dab  ./tasksel_3.68ubuntu2+11.0trisquel6.dsc
09062e3bd89b316621326bbb80224bdfe4b08d98efa82b6697f26f1525d72e39  ./buildlog.txt
1b40d27fc1a02603c1e74d74152bb627f50ea843272c9f95b627038427ef55e0  ./tasksel_3.68ubuntu2+11.0trisquel6_amd64.buildinfo
96eee4462403f243180770c51dddf376d1d2091a4ffa7d1bb7b63f22a1682474  ./tasksel-data_3.68ubuntu2+11.0trisquel6_all.deb
4d6385a7b40eaec92c1b14523224242604c1de3c85fdbc4d69c9a468a642922e  ./tasksel_3.68ubuntu2+11.0trisquel6.tar.xz
e79739f83cb932370476b52ee2189623e79a38aab021a0f3f0475ea3ae4e0276  ./tasksel_3.68ubuntu2+11.0trisquel6_all.deb
+ mkdir published
+ cd published
+ sed -e s,.*/\([^_]*\)_.*,\1,
+ echo ../tasksel-data_3.68ubuntu2+11.0trisquel6_all.deb
+ deb=tasksel-data
+ cut -d' -f2
+ grep ^'
+ apt-get --print-uris --yes download tasksel-data
+ url=http://archive.trisquel.info/trisquel/pool/main/t/tasksel/tasksel-data_3.68ubuntu2%2b11.0trisquel6_all.deb
+ wget -q http://archive.trisquel.info/trisquel/pool/main/t/tasksel/tasksel-data_3.68ubuntu2%2b11.0trisquel6_all.deb
+ sed -e s,.*/\([^_]*\)_.*,\1,
+ echo ../tasksel_3.68ubuntu2+11.0trisquel6_all.deb
+ deb=tasksel
+ cut -d' -f2
+ grep ^'
+ apt-get --print-uris --yes download tasksel
+ url=http://archive.trisquel.info/trisquel/pool/main/t/tasksel/tasksel_3.68ubuntu2%2b11.0trisquel6_all.deb
+ wget -q http://archive.trisquel.info/trisquel/pool/main/t/tasksel/tasksel_3.68ubuntu2%2b11.0trisquel6_all.deb
+ tee ../SHA256SUMS
+ find . -maxdepth 1 -type f
+ sha256sum ./tasksel-data_3.68ubuntu2+11.0trisquel6_all.deb ./tasksel_3.68ubuntu2+11.0trisquel6_all.deb
96eee4462403f243180770c51dddf376d1d2091a4ffa7d1bb7b63f22a1682474  ./tasksel-data_3.68ubuntu2+11.0trisquel6_all.deb
e79739f83cb932370476b52ee2189623e79a38aab021a0f3f0475ea3ae4e0276  ./tasksel_3.68ubuntu2+11.0trisquel6_all.deb
+ cd ..
+ sha256sum -c SHA256SUMS
./tasksel-data_3.68ubuntu2+11.0trisquel6_all.deb: OK
./tasksel_3.68ubuntu2+11.0trisquel6_all.deb: OK
+ echo Package tasksel is reproducible!
Package tasksel is reproducible!
