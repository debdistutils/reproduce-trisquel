+ date
Tue Apr 11 07:55:11 UTC 2023
+ apt-get source --only-source 0ad-data
Reading package lists...
NOTICE: '0ad-data' packaging is maintained in the 'Git' version control system at:
https://salsa.debian.org/games-team/0ad-data.git
Please use:
git clone https://salsa.debian.org/games-team/0ad-data.git
to retrieve the latest (possibly unreleased) updates to the package.
Need to get 1770 MB of source archives.
Get:1 http://archive.trisquel.info/trisquel aramo-backports/main 0ad-data 0.0.26-0ubuntu0.1~wfg1+11.0trisquel1 (dsc) [1961 B]
Get:2 http://archive.trisquel.info/trisquel aramo-backports/main 0ad-data 0.0.26-0ubuntu0.1~wfg1+11.0trisquel1 (tar) [1770 MB]
dpkg-source: info: extracting 0ad-data in 0ad-data-0.0.26
dpkg-source: info: unpacking 0ad-data_0.0.26-0ubuntu0.1~wfg1+11.0trisquel1.tar.gz
Fetched 1770 MB in 2min 14s (13.2 MB/s)
W: Download is performed unsandboxed as root as file '0ad-data_0.0.26-0ubuntu0.1~wfg1+11.0trisquel1.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source 0ad-data
Reading package lists...
Building dependency tree...
Reading state information...
The following NEW packages will be installed:
  autoconf automake autopoint autotools-dev debhelper debugedit dh-autoreconf
  dh-strip-nondeterminism dwz libdebhelper-perl
  libfile-stripnondeterminism-perl libsub-override-perl libtool m4 po-debconf
0 upgraded, 15 newly installed, 0 to remove and 11 not upgraded.
Need to get 3149 kB of archives.
After this operation, 8859 kB of additional disk space will be used.
Get:1 http://archive.trisquel.info/trisquel aramo/main amd64 m4 amd64 1.4.18-5ubuntu2 [199 kB]
Get:2 http://archive.trisquel.info/trisquel aramo/main amd64 autoconf all 2.71-2 [338 kB]
Get:3 http://archive.trisquel.info/trisquel aramo/main amd64 autotools-dev all 20220109.1 [44.9 kB]
Get:4 http://archive.trisquel.info/trisquel aramo/main amd64 automake all 1:1.16.5-1.3 [558 kB]
Get:5 http://archive.trisquel.info/trisquel aramo/main amd64 autopoint all 0.21-4ubuntu4 [422 kB]
Get:6 http://archive.trisquel.info/trisquel aramo/main amd64 libdebhelper-perl all 13.6ubuntu1 [67.2 kB]
Get:7 http://archive.trisquel.info/trisquel aramo/main amd64 libtool all 2.4.6-15build2 [164 kB]
Get:8 http://archive.trisquel.info/trisquel aramo/main amd64 dh-autoreconf all 20 [16.1 kB]
Get:9 http://archive.trisquel.info/trisquel aramo/main amd64 libsub-override-perl all 0.09-2 [9532 B]
Get:10 http://archive.trisquel.info/trisquel aramo/main amd64 libfile-stripnondeterminism-perl all 1.13.0-1 [18.1 kB]
Get:11 http://archive.trisquel.info/trisquel aramo/main amd64 dh-strip-nondeterminism all 1.13.0-1 [5344 B]
Get:12 http://archive.trisquel.info/trisquel aramo/main amd64 debugedit amd64 1:5.0-4build1 [47.2 kB]
Get:13 http://archive.trisquel.info/trisquel aramo/main amd64 dwz amd64 0.14-1build2 [105 kB]
Get:14 http://archive.trisquel.info/trisquel aramo/main amd64 po-debconf all 1.0.21+nmu1 [233 kB]
Get:15 http://archive.trisquel.info/trisquel aramo/main amd64 debhelper all 13.6ubuntu1 [923 kB]
debconf: delaying package configuration, since apt-utils is not installed
Fetched 3149 kB in 1s (2816 kB/s)
Selecting previously unselected package m4.
(Reading database ... (Reading database ... 5%(Reading database ... 10%(Reading database ... 15%(Reading database ... 20%(Reading database ... 25%(Reading database ... 30%(Reading database ... 35%(Reading database ... 40%(Reading database ... 45%(Reading database ... 50%(Reading database ... 55%(Reading database ... 60%(Reading database ... 65%(Reading database ... 70%(Reading database ... 75%(Reading database ... 80%(Reading database ... 85%(Reading database ... 90%(Reading database ... 95%(Reading database ... 100%(Reading database ... 123238 files and directories currently installed.)
Preparing to unpack .../00-m4_1.4.18-5ubuntu2_amd64.deb ...
Unpacking m4 (1.4.18-5ubuntu2) ...
Selecting previously unselected package autoconf.
Preparing to unpack .../01-autoconf_2.71-2_all.deb ...
Unpacking autoconf (2.71-2) ...
Selecting previously unselected package autotools-dev.
Preparing to unpack .../02-autotools-dev_20220109.1_all.deb ...
Unpacking autotools-dev (20220109.1) ...
Selecting previously unselected package automake.
Preparing to unpack .../03-automake_1%3a1.16.5-1.3_all.deb ...
Unpacking automake (1:1.16.5-1.3) ...
Selecting previously unselected package autopoint.
Preparing to unpack .../04-autopoint_0.21-4ubuntu4_all.deb ...
Unpacking autopoint (0.21-4ubuntu4) ...
Selecting previously unselected package libdebhelper-perl.
Preparing to unpack .../05-libdebhelper-perl_13.6ubuntu1_all.deb ...
Unpacking libdebhelper-perl (13.6ubuntu1) ...
Selecting previously unselected package libtool.
Preparing to unpack .../06-libtool_2.4.6-15build2_all.deb ...
Unpacking libtool (2.4.6-15build2) ...
Selecting previously unselected package dh-autoreconf.
Preparing to unpack .../07-dh-autoreconf_20_all.deb ...
Unpacking dh-autoreconf (20) ...
Selecting previously unselected package libsub-override-perl.
Preparing to unpack .../08-libsub-override-perl_0.09-2_all.deb ...
Unpacking libsub-override-perl (0.09-2) ...
Selecting previously unselected package libfile-stripnondeterminism-perl.
Preparing to unpack .../09-libfile-stripnondeterminism-perl_1.13.0-1_all.deb ...
Unpacking libfile-stripnondeterminism-perl (1.13.0-1) ...
Selecting previously unselected package dh-strip-nondeterminism.
Preparing to unpack .../10-dh-strip-nondeterminism_1.13.0-1_all.deb ...
Unpacking dh-strip-nondeterminism (1.13.0-1) ...
Selecting previously unselected package debugedit.
Preparing to unpack .../11-debugedit_1%3a5.0-4build1_amd64.deb ...
Unpacking debugedit (1:5.0-4build1) ...
Selecting previously unselected package dwz.
Preparing to unpack .../12-dwz_0.14-1build2_amd64.deb ...
Unpacking dwz (0.14-1build2) ...
Selecting previously unselected package po-debconf.
Preparing to unpack .../13-po-debconf_1.0.21+nmu1_all.deb ...
Unpacking po-debconf (1.0.21+nmu1) ...
Selecting previously unselected package debhelper.
Preparing to unpack .../14-debhelper_13.6ubuntu1_all.deb ...
Unpacking debhelper (13.6ubuntu1) ...
Setting up po-debconf (1.0.21+nmu1) ...
Setting up libdebhelper-perl (13.6ubuntu1) ...
Setting up m4 (1.4.18-5ubuntu2) ...
Setting up autotools-dev (20220109.1) ...
Setting up autopoint (0.21-4ubuntu4) ...
Setting up autoconf (2.71-2) ...
Setting up dwz (0.14-1build2) ...
Setting up debugedit (1:5.0-4build1) ...
Setting up libsub-override-perl (0.09-2) ...
Setting up automake (1:1.16.5-1.3) ...
update-alternatives: using /usr/bin/automake-1.16 to provide /usr/bin/automake (automake) in auto mode
Setting up libfile-stripnondeterminism-perl (1.13.0-1) ...
Setting up libtool (2.4.6-15build2) ...
Setting up dh-autoreconf (20) ...
Setting up dh-strip-nondeterminism (1.13.0-1) ...
Setting up debhelper (13.6ubuntu1) ...
Processing triggers for man-db (2.10.2-1) ...
+ find . -maxdepth 1 -name 0ad-data* -type d
+ cd ./0ad-data-0.0.26
+ eatmydata env DEB_BUILD_OPTIONS=noautodbgsym dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package 0ad-data
dpkg-buildpackage: info: source version 0.0.26-0ubuntu0.1~wfg1+11.0trisquel1
dpkg-buildpackage: info: source distribution aramo-backports
dpkg-buildpackage: info: source changed by Trisquel GNU/Linux developers <trisquel-devel@listas.trisquel.info>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 debian/rules clean
dh clean
   dh_clean
 dpkg-source -b .
dpkg-source: warning: no source format specified in debian/source/format, see dpkg-source(1)
dpkg-source: warning: native package version may not have a revision
dpkg-source: info: using source format '1.0'
dpkg-source: info: building 0ad-data in 0ad-data_0.0.26-0ubuntu0.1~wfg1+11.0trisquel1.tar.gz
dpkg-source: info: building 0ad-data in 0ad-data_0.0.26-0ubuntu0.1~wfg1+11.0trisquel1.dsc
 debian/rules binary
dh binary
   dh_update_autotools_config
   dh_autoreconf
   create-stamp debian/debhelper-build-stamp
   dh_prep
   debian/rules override_dh_install
make[1]: Entering directory '/build/0ad-data/0ad-data-0.0.26'
dh_install --exclude=LICENSE
make[1]: Leaving directory '/build/0ad-data/0ad-data-0.0.26'
   dh_installdocs
   dh_installchangelogs
   dh_perl
   debian/rules override_dh_link
make[1]: Entering directory '/build/0ad-data/0ad-data-0.0.26'
# Remove embedded fonts and use system copies instead
rm -f /build/0ad-data/0ad-data-0.0.26/debian/0ad-data-common/usr/share/games/0ad/tools/fontbuilder/fonts/{DejaVuSansMono,Free*,texgyrepagella*}.{ttf,otf}
dh_link
make[1]: Leaving directory '/build/0ad-data/0ad-data-0.0.26'
   dh_strip_nondeterminism
   dh_compress
   dh_fixperms
   dh_missing
   dh_installdeb
   dh_gencontrol
   dh_md5sums
   debian/rules override_dh_builddeb
make[1]: Entering directory '/build/0ad-data/0ad-data-0.0.26'
dh_builddeb -- -Zxz
pkgstripfiles: processing control file: debian/0ad-data/DEBIAN/control, package 0ad-data, directory debian/0ad-data
pkgstripfiles: processing control file: debian/0ad-data-common/DEBIAN/control, package 0ad-data-common, directory debian/0ad-data-common
INFO: pkgstripfiles: waiting for lock (0ad-data-common) ...
pkgstripfiles: Truncating usr/share/doc/0ad-data/changelog.Debian.gz to topmost ten records
pkgstripfiles: Skipping PNG optimization for package in games section.
dpkg-deb: building package '0ad-data' in '../0ad-data_0.0.26-0ubuntu0.1~wfg1+11.0trisquel1_all.deb'.
pkgstripfiles: Truncating usr/share/doc/0ad-data-common/changelog.Debian.gz to topmost ten records
pkgstripfiles: Skipping PNG optimization for package in games section.
dpkg-deb: building package '0ad-data-common' in '../0ad-data-common_0.0.26-0ubuntu0.1~wfg1+11.0trisquel1_all.deb'.
make[1]: Leaving directory '/build/0ad-data/0ad-data-0.0.26'
 dpkg-genbuildinfo -O../0ad-data_0.0.26-0ubuntu0.1~wfg1+11.0trisquel1_amd64.buildinfo
 dpkg-genchanges -O../0ad-data_0.0.26-0ubuntu0.1~wfg1+11.0trisquel1_amd64.changes
dpkg-genchanges: info: including full source code in upload
 dpkg-source --after-build .
dpkg-buildpackage: info: full upload; Debian-native package (full source is included)
+ date
Tue Apr 11 08:15:23 UTC 2023
+ cd ..
+ ls -la
total 3074488
drwxr-xr-x 3 root root       4096 Apr 11 08:15 .
drwxr-xr-x 3 root root       4096 Apr 11 07:55 ..
drwxr-xr-x 4 root root       4096 Oct  1  2022 0ad-data-0.0.26
-rw-r--r-- 1 root root     779112 Apr 11 08:13 0ad-data-common_0.0.26-0ubuntu0.1~wfg1+11.0trisquel1_all.deb
-rw-r--r-- 1 root root       1078 Apr 11 08:04 0ad-data_0.0.26-0ubuntu0.1~wfg1+11.0trisquel1.dsc
-rw-r--r-- 1 root root 1769997037 Apr 11 08:04 0ad-data_0.0.26-0ubuntu0.1~wfg1+11.0trisquel1.tar.gz
-rw-r--r-- 1 root root 1377440908 Apr 11 08:14 0ad-data_0.0.26-0ubuntu0.1~wfg1+11.0trisquel1_all.deb
-rw-r--r-- 1 root root       7131 Apr 11 08:14 0ad-data_0.0.26-0ubuntu0.1~wfg1+11.0trisquel1_amd64.buildinfo
-rw-r--r-- 1 root root       2544 Apr 11 08:15 0ad-data_0.0.26-0ubuntu0.1~wfg1+11.0trisquel1_amd64.changes
-rw-r--r-- 1 root root      10309 Apr 11 08:15 buildlog.txt
+ find . -maxdepth 1 -type f
+ sha256sum ./0ad-data_0.0.26-0ubuntu0.1~wfg1+11.0trisquel1_amd64.buildinfo ./0ad-data_0.0.26-0ubuntu0.1~wfg1+11.0trisquel1_all.deb ./buildlog.txt ./0ad-data_0.0.26-0ubuntu0.1~wfg1+11.0trisquel1.dsc ./0ad-data-common_0.0.26-0ubuntu0.1~wfg1+11.0trisquel1_all.deb ./0ad-data_0.0.26-0ubuntu0.1~wfg1+11.0trisquel1.tar.gz ./0ad-data_0.0.26-0ubuntu0.1~wfg1+11.0trisquel1_amd64.changes
be09890a1f99a16c30aa519d0efbbc1047e44caff04fc218e86178b92d1ff76a  ./0ad-data_0.0.26-0ubuntu0.1~wfg1+11.0trisquel1_amd64.buildinfo
eb93296a3c5438683bbf42a75c06bd4c25fd6383f84fa3d3e8d70341016337db  ./0ad-data_0.0.26-0ubuntu0.1~wfg1+11.0trisquel1_all.deb
957822945dff7980d8a6c030579ce275d3070424d7aed9f1f10585acbe9c09e6  ./buildlog.txt
2ec2f2a2bc9fa47303937c4f71e84a69eff45154366c8e94bc91a5b909242c6f  ./0ad-data_0.0.26-0ubuntu0.1~wfg1+11.0trisquel1.dsc
aebcf98b69a3c8af1c38f6045d85611433501360fbdd1e9f2da2f4dc2aad980f  ./0ad-data-common_0.0.26-0ubuntu0.1~wfg1+11.0trisquel1_all.deb
2073ce630519807cf983ca364c4004c71c8214b4eacaaf28caf632176da6a4b0  ./0ad-data_0.0.26-0ubuntu0.1~wfg1+11.0trisquel1.tar.gz
7205e0168e4a9e0738fad26d076324898cec6f3d8aff2dee9f13961432763655  ./0ad-data_0.0.26-0ubuntu0.1~wfg1+11.0trisquel1_amd64.changes
+ mkdir published
+ cd published
+ cd ../
+ ls 0ad-data-common_0.0.26-0ubuntu0.1~wfg1+11.0trisquel1_all.deb 0ad-data_0.0.26-0ubuntu0.1~wfg1+11.0trisquel1_all.deb *.udeb
ls: cannot access '*.udeb': No such file or directory
+ wget -q http://archive.trisquel.info/trisquel/pool/main/0/0ad-data/0ad-data-common_0.0.26-0ubuntu0.1~wfg1+11.0trisquel1_all.deb
+ wget -q http://archive.trisquel.info/trisquel/pool/main/0/0ad-data/0ad-data_0.0.26-0ubuntu0.1~wfg1+11.0trisquel1_all.deb
+ tee ../SHA256SUMS
+ find . -maxdepth 1 -type f
+ sha256sum ./0ad-data_0.0.26-0ubuntu0.1~wfg1+11.0trisquel1_all.deb ./0ad-data-common_0.0.26-0ubuntu0.1~wfg1+11.0trisquel1_all.deb
eb93296a3c5438683bbf42a75c06bd4c25fd6383f84fa3d3e8d70341016337db  ./0ad-data_0.0.26-0ubuntu0.1~wfg1+11.0trisquel1_all.deb
aebcf98b69a3c8af1c38f6045d85611433501360fbdd1e9f2da2f4dc2aad980f  ./0ad-data-common_0.0.26-0ubuntu0.1~wfg1+11.0trisquel1_all.deb
+ cd ..
+ sha256sum -c SHA256SUMS
./0ad-data_0.0.26-0ubuntu0.1~wfg1+11.0trisquel1_all.deb: OK
./0ad-data-common_0.0.26-0ubuntu0.1~wfg1+11.0trisquel1_all.deb: OK
+ echo Package 0ad-data is reproducible!
Package 0ad-data is reproducible!
